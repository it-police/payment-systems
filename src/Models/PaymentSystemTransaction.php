<?php

namespace ITPolice\PaymentSystems\Models;

use \Illuminate\Database\Eloquent\Model;
use ITPolice\PaymentSystems\Interfaces\ResponseInterface;
use ITPolice\PaymentSystems\Interfaces\ServiceInterface;
use ITPolice\PaymentSystems\PaymentSystemFactory;
use ITPolice\PaymentSystems\Traits\ServiceTrait;

/**
 * Class PaymentSystemTransaction
 * @package ITPolice\PaymentSystems\Models
 * @property $id
 * @property $transaction_id
 * @property $system
 * @property $operation
 * @property $merchant
 * @property $client_id
 * @property $contract_id
 * @property $extra
 * @property $status
 * @property $error
 * @property $amount
 * @property $email
 * @property $phone
 * @property $card_token
 */
class PaymentSystemTransaction extends Model
{
    protected $table = 'payment_system_transactions';

    protected $fillable = ['*'];

    protected $casts = [
        'extra' => 'object'
    ];

    /**
     * @param ServiceInterface|ServiceTrait $factory
     */
    public function setFactory($factory)
    {
        $this->system = $factory::getSystemCode();
        $this->operation = $factory->getOperation();
        $this->merchant = $factory->getMerchant();
        $this->phone = $factory->getPhone();
        $this->email = $factory->getEmail();
        return $this;
    }

    /**
     * @param ResponseInterface $response
     */
    public function setResponse($response)
    {
        $this->transaction_id = $response->id();
        return $this;
    }

    public function addExtra($key, $value)
    {
        $extra = (array) $this->extra;
        $extra[$key] = $value;
        $this->extra = $extra;
    }

    public function getExtra($key) {
        $extra = (array) $this->extra;
        if(empty($extra) || !isset($extra[$key])) return null;
        return $extra[$key];
    }
}
