<?php

namespace ITPolice\PaymentSystems\Models;

class BankCard
{
    public $token;
    public $bin;
    public $pan;
    public $month;
    public $year;
    public $holder;
    public $bankName;
    public $payout_token;
}
