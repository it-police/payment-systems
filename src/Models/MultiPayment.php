<?php

namespace ITPolice\PaymentSystems\Models;

class MultiPayment
{
    /**
     * @var float
     */
    public $amount;
    /**
     * @var float
     */
    public $fee;
    /**
     * @var string
     */
    public $description;
    /**
     * @var mixed
     */
    public $merchant;
    /**
     * @var mixed
     */
    public $reference;

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return MultiPayment
     */
    public function setAmount(float $amount): MultiPayment
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return float
     */
    public function getFee(): float
    {
        return $this->fee;
    }

    /**
     * @param float $fee
     * @return MultiPayment
     */
    public function setFee(float $fee): MultiPayment
    {
        $this->fee = $fee;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return MultiPayment
     */
    public function setDescription(string $description): MultiPayment
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchant()
    {
        return $this->merchant;
    }

    /**
     * @param mixed $merchant
     * @return MultiPayment
     */
    public function setMerchant($merchant): MultiPayment
    {
        $this->merchant = $merchant;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return (string) $this->reference;
    }

    /**
     * @param mixed $reference
     * @return MultiPayment
     */
    public function setReference($reference): MultiPayment
    {
        $this->reference = $reference;
        return $this;
    }


}
