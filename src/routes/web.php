<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'pay-system',
    'namespace'  => 'App\Http\Controllers',
], function () {
    Route::group(['prefix' => '{system}'], function () {
        Route::any('callback', 'PaymentSystemController@callback')->name('pay-system.callback');
        Route::any('callback-test', 'PaymentSystemController@callbackTest')->name('pay-system.callback-test');
    });
});

Route::group([
    'prefix' => 'pay-system',
    'namespace'  => 'ITPolice\PaymentSystems\Controllers',
], function () {
    Route::group(['prefix' => 'dev'], function () {
        Route::any('transaction', 'DevController@transaction')->name('pay-system.dev.transaction');
    });
});
