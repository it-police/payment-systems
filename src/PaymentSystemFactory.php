<?php

namespace ITPolice\PaymentSystems;


use ITPolice\PaymentSystems\Interfaces\ServiceInterface;
use ITPolice\PaymentSystems\Systems\Dev\Service as Dev;
use ITPolice\PaymentSystems\Systems\MandarinBank\Service as MandarinBank;
use ITPolice\PaymentSystems\Systems\Best2Pay\Service as Best2Pay;
use ITPolice\PaymentSystems\Systems\SpareBest2Pay\Service as SpareBest2Pay;
use ITPolice\PaymentSystems\Systems\ImPay\Service as ImPay;
use ITPolice\PaymentSystems\Systems\TarlanPayments\Service as TarlanPayments;
use ITPolice\PaymentSystems\Systems\GreenLeavesPay\Service as GreenLeavesPay;
use ITPolice\PaymentSystems\Systems\AlfaBank\Service as AlfaBank;
use ITPolice\PaymentSystems\Systems\QiwiKz\Service as QiwiKz;
use ITPolice\PaymentSystems\Systems\Payselection\Service as Payselection;
use ITPolice\PaymentSystems\Traits\ServiceTrait;

final class PaymentSystemFactory
{
    /**
     * @param $type
     * @return ServiceInterface|ServiceTrait
     * @throws \Exception
     */
    public static function factory($type)
    {
        $paymentSystems = [
            MandarinBank::class,
            Best2Pay::class,
            ImPay::class,
            Dev::class,
            TarlanPayments::class,
            GreenLeavesPay::class,
            AlfaBank::class,
            QiwiKz::class,
            Payselection::class,
            SpareBest2Pay::class,
        ];

        if(env('PAY_SYSTEM_USE_DEV_SYSTEM', false)) {
            return new Dev();
        }

        foreach ($paymentSystems as $paymentSystem) {
            $class = new $paymentSystem();
            if ($class::getSystemCode() == $type) {
                return $class;
            }
        }

        throw new \Exception('Incorrect pay system type');
    }
}
