<?php


namespace ITPolice\PaymentSystems\Traits;


trait CallbackRequestTrait
{
    /**
     * ID мерчанта
     * @var
     */
    protected $merchantId;

    /**
     * Секретный ключ
     * @var
     */
    protected $secret;
    protected $callbackSecret;
    protected $post = [];

    private $request = [];

    public function __construct(array $request) {
        $this->request = $request;
    }

    public function setMerchant($merchantId)
    {
        $this->merchantId = $merchantId;
        return $this;
    }

    public function setSecret($secret)
    {
        $this->secret = $secret;
        return $this;
    }
    
    public function setCallbackSecret($secret)
    {
        $this->callbackSecret = $secret;
        return $this;
    }

    public function setPost($post) {
        $this->post = $post;
        return $this;
    }

    public function setGet($get) {
        $this->get = $get;
        return $this;
    }

    public function isStatusSuccess()
    {
        return $this->status() == self::STATUS_SUCCESS;
    }

    public function isStatusError()
    {
        return $this->status() == self::STATUS_ERROR;
    }

    public function isActionPay()
    {
        return $this->action() == self::ACTION_PAY;
    }

    public function isActionReverse()
    {
        return $this->action() == self::ACTION_REVERSE;
    }

    public function isActionPayout()
    {
        return $this->action() == self::ACTION_PAYOUT;
    }

    public function isActionCardBinding()
    {
        return $this->action() == self::ACTION_CARD_BINDING;
    }

    public function checkSignTest() {
        return $this->checkSign();
    }
}
