<?php

namespace ITPolice\PaymentSystems\Traits;


use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Models\MultiPayment;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;

/**
 * Trait PaymentSystemTrait
 * @package ITPolice\PaymentSystems
 */
trait ServiceTrait
{
    public $callbackRequest;
    protected $model;
    /**
     * ID мерчанта
     * @var
     */
    protected $merchantId;
    /**
     * Секретный ключ
     * @var
     */
    protected $secret;
    protected $operation;
    protected $email;
    protected $phone;
    protected $userId;
    protected $contractName;
    protected $fio;
    protected $returnUrl;
    protected $defaultReturnUrl;
    protected $callbackUrl;
    protected $defaultEmail;
    /**
     * @var MultiPayment[]
     */
    protected $multiPayments = [];
    protected $callbackSecret;

    public function __construct()
    {
        $this->setDefaultReturnUrl(env('PAY_SYSTEM_DEFAULT_RETURN_URL', env('APP_URL')));
        $this->setDefaultEmail(env('PAY_SYSTEM_DEFAULT_EMAIL', env('MAIL_FROM_ADDRESS')));
        $this->setCallbackUrl(route('pay-system.callback', ['system' => self::$systemCode]));
        if(self::$systemCode == 'dev' && env('PAY_SYSTEM_DEV_CALLBACK_URL')) {
            $this->setCallbackUrl(env('PAY_SYSTEM_DEV_CALLBACK_URL', 'http://nginx:80/pay-system/dev/callback'));
        }
        $this->setCredentials();
    }

    public function setDefaultReturnUrl($url)
    {
        $this->defaultReturnUrl = $url;
        return $this;
    }

    public function setDefaultEmail($email)
    {
        $this->defaultEmail = $email;
        return $this;
    }

    public function setCallbackUrl($url)
    {
        $this->callbackUrl = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCallbackUrl()
    {
        return $this->callbackUrl;
    }


    public static function getSystemCode()
    {
        if (!self::$systemCode) throw new \Exception('Не указан код платежного сервиса');
        return self::$systemCode;
    }

    public function setReturnUrl($url)
    {
        $this->returnUrl = $url;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $this->preparePhone($phone);
        return $this;
    }

    /**
     * Удаление лишних символов из номера телефона
     * @param $phone
     * @return false|string
     */
    public function preparePhone($phone)
    {
        if ($phone != '') {
            $phone = preg_replace("/[^0-9]+/", '', $phone);
            return $phone;
        }
        return false;
    }

    /**
     * @return CallbackRequestTrait|CallbackRequestInterface
     */
    public function getCallbackRequest()
    {
        return $this->callbackRequest;
    }

    /**
     * @param null $transactionId
     * @return PaymentSystemTransaction
     */
    public function getModel($transactionId = null)
    {
        if ($transactionId) {
            $query = PaymentSystemTransaction::query()
                ->where('transaction_id', $transactionId)
                ->where('system', self::$systemCode);

            if (!$this->model || ($this->model && $this->model->transaction_id != $transactionId)) {
                $this->model = $query->first();
                if ($this->model) {
                    $this->setModel($this->model);
                }
            }
        }

        return $this->model;
    }

    public function setModel(PaymentSystemTransaction $model)
    {
        $this->model = $model;

        // мерчант
        $this->setMerchant($model->merchant, true);
        return $this;
    }

    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param $operation
     * @param PaymentSystemTransaction $previousModel
     */
    public function setOperation($operation, $previousModel = null)
    {
        $this->operation = $operation;
        $this->setCredentials($operation);
        if ($previousModel) {
            $this->callbackAfterSetOperation($previousModel);
        }
        return $this;
    }

    /**
     * @param string $operation
     */
    public function setCredentials($operation = '')
    {
        $merchantId = $this->getMerchantByOperation($operation);
        $this->setMerchant($merchantId);
        return $this;
    }

    /**
     * @param $operation
     */
    public function getMerchantByOperation($operation)
    {
        $system = self::$systemCode;
        $merchantId = env("PAY_SYSTEM_MERCHANT_{$system}_{$operation}", env("PAY_SYSTEM_MERCHANT_{$system}", 1));
        return $merchantId;
    }

    /**
     * @param $merchantId
     * @param $withSecret
     */
    public function setMerchant($merchantId = null, $withSecret = true)
    {
        if ($merchantId) {
            $this->merchantId = $merchantId;
            if ($this->callbackRequest) $this->callbackRequest->setMerchant($merchantId);
            if ($withSecret) {
                // пароль
                $secret = $this->getSecretByMerchant();
                $this->setSecret($secret);
                // токен callback
                $callbackSecret = $this->getCallbackSecretByMerchant();
                $this->setCallbackSecret($callbackSecret);
            }
        }
        return $this;
    }

    public function getSecretByMerchant()
    {
        $system = self::$systemCode;
        $merchantId = $this->merchantId;
        return env("PAY_SYSTEM_SECRET_{$system}_{$merchantId}", env("PAY_SYSTEM_SECRET_{$system}"));
    }

    /**
     * @param $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
        if ($this->callbackRequest) $this->callbackRequest->setSecret($secret);
        return $this;
    }

    public function getCallbackSecretByMerchant()
    {
        $system = self::$systemCode;
        $merchantId = $this->merchantId;
        return env("PAY_SYSTEM_CALLBACK_SECRET_{$system}_{$merchantId}", env("PAY_SYSTEM_CALLBACK_SECRET_{$system}"));
    }

    public function setCallbackSecret($callbackSecret)
    {
        $this->callbackSecret = $callbackSecret;
        if ($this->callbackRequest) $this->callbackRequest->setCallbackSecret($callbackSecret);
        return $this;
    }

    public function getMerchant()
    {
        return $this->merchantId;
    }

    public function getExtra($key)
    {
        return @$this->model->extra->{$key};
    }

    /**
     * Расчёт суммы комиссии
     * @param $amount - Сумма оплаты
     * @param $percent - Процент
     * @param $type - Тип расчета
     */
    public function getCommissionAmount($amount, $percent, $type = null)
    {
        $commission = 0;
        if ($percent) {
            /**
             * пример:
             * сумма = 100р
             * процент = 2.5
             */
            if ($type === null) {
                if ($this instanceof \ITPolice\PaymentSystems\Systems\Best2Pay\Service) {
                    $type = 2;
                } else {
                    $type = 1;
                }
            }
            switch ($type) {
                case 1:
                    // сумма процентов = 2.57
                    $amountWithCommission = ceil(($amount / (1 - ($percent / 100))) * 100) / 100;
                    $commission = $amountWithCommission - $amount;
                    break;
                case 2:
                    // сумма процентов = 2.5
                    $commission = round(($amount / 100) * $percent, 2);
                    break;
                default:
                    throw new \Exception('Не корректный тип расчета процентов', 1000);
            }
        }
        return $commission;
    }

    /**
     * @return mixed
     */
    public function getContractName()
    {
        return $this->contractName;
    }

    /**
     * @return mixed
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * @param mixed $contractName
     */
    public function setContractName($contractName)
    {
        $this->contractName = $contractName;
        return $this;
    }

    /**
     * @param mixed $fio
     */
    public function setFio($fio)
    {
        $this->fio = $fio;
        return $this;
    }

    /**
     * @param float $amount
     * @param mixed $merchant
     * @param string $description
     * @param mixed $reference
     * @param float $fee
     * @return $this
     */
    public function addMultiPayment(float $amount, $merchant, string $description = '', $reference = '', float $fee = 0)
    {
        $multiPayment = new MultiPayment();
        $multiPayment
            ->setAmount($amount)
            ->setFee($fee)
            ->setDescription($description)
            ->setReference($reference)
            ->setMerchant($merchant);

        $this->multiPayments[] = $multiPayment;
        return $this;
    }
}
