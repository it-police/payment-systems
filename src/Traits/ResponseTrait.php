<?php

namespace ITPolice\PaymentSystems\Traits;


use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;

/**
 * Trait ResponseTrait
 * @package ITPolice\PaymentSystems
 */
trait ResponseTrait
{
    private $id;
    private $link;
    private $success;
    private $errorMessage;
    private $isNull;
    private $response;

    public function __construct($response)
    {
        $this->response = $response;
        $this->isNull = !is_object($response);
        $this->parse($response);
    }

    public function toArray(): array
    {
        return (array)$this->response;
    }

    public function toJson(): object
    {
        return (object)$this->response;
    }

    public function isNull(): bool
    {
        return $this->isNull;
    }

    public function errorMessage(): string
    {
        return (string)$this->errorMessage;
    }

    public function link()
    {
        return $this->link;
    }

    public function success(): bool
    {
        return (bool) $this->success;
    }

    public function id()
    {
        return $this->id;
    }

    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    
    public function getResponse() {
        return $this->response;
    }
}
