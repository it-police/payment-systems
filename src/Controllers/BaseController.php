<?php

namespace ITPolice\PaymentSystems\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use ITPolice\PaymentSystems\PaymentSystemFactory;

class BaseController extends Controller
{
    public function callback(Request $request, $system)
    {
        // логирование запроса
        Log::debug("{$system} callback", [$_POST, $_GET, $request->getContent()]);

        //$_POST = json_decode('', true);

        //dump($request->getContent());

        //dump($system);
        // получаем систему
        $factory = PaymentSystemFactory::factory($system);
        if (!$factory) throw new \Exception('Payment system not defined');

        $request = $factory
            ->setCallbackRequest($request, $_POST, $_GET)
            ->getCallbackRequest();

        // получаем ID транзакции
        $transactionId = $request->transactionId();

        $loopIterator = 3;
        loop:

        // получаем модель в БД
        $model = $factory->getModel($transactionId);

        // (Костыль) для случаев, когда callback приходит раньше,
        // чем ответ от прямого запроса к платежке
        // и в бд нет записи с транзакцией
        if (!$model && --$loopIterator != 0) {
            sleep(10);
            goto loop;
        }

        //dump($request, $model, $transactionId);
        if ($model) {
            // получаем тип операции
            $operation = $model->operation;

            // проверяем подпись
            if (!$request->checkSign())
                throw new \Exception('Invalid sign');

            // проверяем существует ли метод
            if (!method_exists($this, $operation))
                throw new \Exception("Method {$operation} not exists");

            // обрабатываем callback
            $response = $this->{$operation}($factory);

            // если запрос обработан
            if ($response === true) {
                // сохраняем статус
                $model->status = $request->status();
                if ($request->isStatusError()) {
                    $errorMessage = $request->errorMessage();
                    $errorCode = $request->errorCode();
                    $model->error = "{$errorCode}:{$errorMessage}";
                }
                $model->save();

                // Если это была успешная привязка карты и включен возврат - возвращаем
                if ($request->isActionCardBinding() && $request->isStatusSuccess()) {
                    if (env('PAY_SYSTEM_CARD_BINDING_REVERSE', false)) {
                        $factory->reverseCardBinding($model);
                    }
                }

                // возвращаем ответ
                echo $factory->responseCallback();
            } else {
                // Если это операция возврата денежных средств,
                // то отправляем ответ в платежку о том что мы приняли callback
                if($request->isActionReverse()) {
                    echo $factory->responseCallback();
                }
            }
        }

    }

    public function callbackTest(Request $request, $system)
    {
        //dump($system);
        // получаем систему
        $factory = PaymentSystemFactory::factory($system);
        if (!$factory) throw new \Exception('Payment system not defined');

        $request = $factory
            ->setCallbackRequest($request, $_POST, $_GET)
            ->getCallbackRequest();

        // получаем ID транзакции
        $transactionId = $request->transactionId();

        // получаем модель в БД
        $model = $factory->getModel($transactionId);

        //dump($request, $model, $transactionId);

        // проверяем подпись
        if (!$request->checkSignTest())
            dump('Invalid sign');
        else
            dump('IT`S OK');

    }
}
