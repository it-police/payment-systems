<?php

namespace ITPolice\PaymentSystems\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Str;
use ITPolice\PaymentSystems\PaymentSystemFactory;

class DevController extends Controller
{
    public function transaction(Request $request)
    {
        $postParams = $request->all();
        $factory = new \ITPolice\PaymentSystems\Systems\Dev\Service();
        $factory->sendCurl($postParams);
        return view('pay-systems::dev-transaction', ['postParams' => $postParams]);
    }
}
