<?php

namespace ITPolice\PaymentSystems\Interfaces;

use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;
use ITPolice\PaymentSystems\Systems\MandarinBank\Response;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;
use Illuminate\Http\Request;

interface ServiceInterface
{

    /**
     * Метод привязки карты
     * @return Response
     * @throws \Exception
     */
    public function cardBinding();

    /**
     * Выдача средств по токену карты
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function payoutByToken($orderId, $amount, $cardToken, $description = '');


    /**
     * Оплата
     * @param $orderId
     * @param float $amount
     * @param string $description
     * @param float $fee
     * @return Response
     * @throws \Exception
     */
    public function pay($orderId, $amount, $description = '', $fee = 0);

    /**
     * todo
     * Повторное списание средств на основе ID обычной оплаты
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function recurring($orderId, $amount, $cardToken, $description = '');

    /**
     * Безакцептное списание средств по токену
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function recurringByToken($orderId, $amount, $cardToken, $description = '');

    /**
     * Обрабатываем параметры возвращаемого callback и сохраняем объект CallbackRequest
     * @param Request $request
     * @param array $POST
     * @param array $GET
     */
    public function setCallbackRequest(Request $request, $POST = null, $GET = null);

    /**
     * Получаем объект CallbackRequest
     * @return CallbackRequestInterface|CallbackRequestTrait
     */
    public function getCallbackRequest();

    /**
     * Возвращаем ответ для платедной системы после успешной обработки callback
     * @return string
     */
    public function responseCallback(): string;

    /**
     * Выполняем метод после вызова функции setOperation, в случае если в аргументы передана модель PaymentSystemTransaction
     * (используется для правильного определения merchantId исходя из предыдущей операции)
     * @param PaymentSystemTransaction $previousModel
     * @return string
     */
    public function callbackAfterSetOperation(PaymentSystemTransaction $previousModel);

    /**
     * Проверка возможности использования сплит платежей
     * @return bool
     */
    public function hasMultiPayments(): bool;

    /**
     * Получение карт пользователя по приему
     * @return BankCard[]
     */
    public function getCardsPayIn(): array;

    /**
     * Получение карт пользователя по выводу
     * @return BankCard[]
     */
    public function getCardsPayOut(): array;

    /**
     * Возврат средств за привязку карты
     *
     * @param PaymentSystemTransaction $model
     *
     * @return mixed
     */
    public function reverseCardBinding(PaymentSystemTransaction $model);

    /**
     * Получение информации о заказе
     * 
     * @param string $orderId
     *
     * @return mixed
     */
    public function orderInfo(string $orderId);
}
