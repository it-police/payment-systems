<?php

namespace ITPolice\PaymentSystems\Interfaces;

use ITPolice\PaymentSystems\Models\BankCard;

interface CallbackRequestInterface
{
    const STATUS_NEW = 'new';
    const STATUS_SENT = 'sent';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    const ACTION_PAY = 'pay';
    const ACTION_PAYOUT = 'payout';
    const ACTION_CARD_BINDING = 'card_binding';
    const ACTION_REVERSE = 'reverse'; // Возврат

    public function __construct(array $request);

    /**
     * Метод проверки подписи
     * @return bool
     */
    public function checkSign();

    /**
     * Получаем ID транзакции из запроса
     * @param array $request
     * @return mixed
     */
    public function transactionId();

    public function status();

    public function action();

    public function errorMessage(): string;

    public function errorCode();

    public function amount(): float;

    public function bankCard(): BankCard;

    public function isStopError(): bool;

    public function isFailedError(): bool;
}
