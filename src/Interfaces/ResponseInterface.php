<?php

namespace ITPolice\PaymentSystems\Interfaces;

interface ResponseInterface
{
    public function __construct($response);

    public function parse($response);

    public function link();

    public function success(): bool;

    public function id();

    public function toArray(): array;

    public function toJson(): object;

    public function isNull(): bool;

    public function errorMessage(): string;
}
