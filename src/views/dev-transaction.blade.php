<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .wrap {
            padding: 50px;
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: center;
        }
        .table {
            margin-bottom: 30px;
            font-size: 14px;
            border: 1px solid;
            max-width: 300px;
            padding: 0;
        }
        .table tr {
            padding: 0;
            margin: 0;
        }
        .table tr td {
            padding: 5px;
            margin: 0;
            border: 1px solid;
        }
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }

        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 64px;
            height: 64px;
            margin: 8px;
            border: 8px solid #333;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #333 transparent transparent transparent;
        }

        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }

        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }

        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }

        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

    </style>
</head>
<body>
<div class="wrap">
    <table class="table">
        <tr>
            <td>Номер транзакции</td>
            <td>{{@$postParams['id']}}</td>
        </tr>
        <tr>
            <td>Описание</td>
            <td>{{@$postParams['description']}}</td>
        </tr>
        <tr>
            <td>Сумма</td>
            <td>{{@$postParams['amount']}}</td>
        </tr>
        <tr>
            <td>Номер заказа</td>
            <td>{{@$postParams['orderId']}}</td>
        </tr>
    </table>
    <div class="lds-ring">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<script>
    window.setTimeout(function(){
        window.location.href = "{{$postParams['returnUrl']}}";
    }, 4000);
</script>
</body>
</html>
