<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;

class CreatePaymentSystemTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_system_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_id');
            $table->string('system');
            $table->string('operation');
            $table->string('merchant');
            $table->integer('client_id')->nullable();
            $table->integer('contract_id')->nullable();
            $table->string('card_token')->nullable();
            $table->longText('extra')->nullable();
            $table->enum('status', [
                CallbackRequestInterface::STATUS_NEW,
                CallbackRequestInterface::STATUS_SENT,
                CallbackRequestInterface::STATUS_SUCCESS,
                CallbackRequestInterface::STATUS_ERROR,
            ])->default(CallbackRequestInterface::STATUS_NEW);
            $table->string('error')->nullable();
            $table->float('amount')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_system_transactions');
    }
}
