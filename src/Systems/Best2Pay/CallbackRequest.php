<?php

namespace ITPolice\PaymentSystems\Systems\Best2Pay;

use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;

class CallbackRequest implements CallbackRequestInterface
{
    use CallbackRequestTrait;

    /**
     * Метод проверки подписи
     * @param array $request
     * @return bool
     */
    public function checkSign()
    {
        $request = $this->request;
        $sign = $request['signature'];
        unset($request['signature']);
        $signature = implode($request).$this->secret;
        return base64_encode(md5($signature)) === $sign;
    }

    public function transactionId()
    {
        $transactionId = @$this->request['id'];
        $object_type = @$this->request['type'];
        $reference = @$this->request['reference'];
        if($this->action() == self::ACTION_CARD_BINDING) {
            $transactionId = @$this->request['order_id'];
        } elseif($this->action() == self::ACTION_PAY) {
            if($reference && preg_match("/".Service::MULTI_PAYMENT_PREFIX."/", $reference)) {
                $transactionId = str_replace(Service::MULTI_PAYMENT_PREFIX , '', $reference);
            } else {
                if($object_type == 'RECURRING') {
                    $transactionId = @$this->request['id'];
                } else {
                    $transactionId = @$this->request['order_id'];
                }
            }
        } elseif($this->action() == self::ACTION_PAYOUT) {
            $transactionId = @$this->request['order_id'];
        } elseif($this->action() == self::ACTION_REVERSE) {
            $transactionId = @$this->request['order_id'];
        }

        return $transactionId;
    }

    public function action()
    {
        $object_type = @$this->request['type'];
        if ($object_type == 'RECURRING' || $object_type == 'PURCHASE') {
            return self::ACTION_PAY;
        } elseif ($object_type == 'P2PCREDIT') {
            return self::ACTION_PAYOUT;
        } elseif ($object_type == 'AUTHORIZE') {
            return self::ACTION_CARD_BINDING;
        } elseif ($object_type == 'REVERSE') {
            return self::ACTION_REVERSE;
        }
        return null;
    }

    public function errorMessage(): string
    {
        return (string) (@$this->request['description'] ?? @$this->request['message']);
    }

    public function amount(): float
    {
        return (float)@$this->request['amount'] / 100;
    }

    public function bankCard(): BankCard
    {
        $bankCard = new BankCard();

        $token = $this->request['token'];
        $cardBin = mb_substr($this->request['pan'], 0, 6);
        $cardPan = mb_substr($this->request['pan'], -4);
        list($cardMonth, $cardYear) = explode('/', $this->request['expdate']);
        $cardMonth = str_pad($cardMonth, 2, '0', STR_PAD_LEFT);
        $cardYear = (int)$cardYear;
        $cardHolder = $this->request['name'];
        $bankName = @$this->request['bin_issuer'];

        $bankCard->token = $token;
        $bankCard->bin = $cardBin;
        $bankCard->pan = $cardPan;
        $bankCard->month = $cardMonth;
        $bankCard->year = $cardYear;
        $bankCard->holder = $cardHolder;
        $bankCard->bankName = $bankName;

        return $bankCard;
    }

    public function isStopError(): bool
    {
        switch ((int)$this->errorCode()) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 7:
            case 8:
            case 22:
                return true;
        }
        return false;
    }

    public function status()
    {
        $status = @$this->request['state'];
        switch ($status) {
            case 'APPROVED':
                return self::STATUS_SUCCESS;
            case 'REJECTED':
            case 'ERROR':
            //case 'TIMEOUT':
                return self::STATUS_ERROR;
            default:
                return $status;
        }
    }

    public function errorCode()
    {
        return @$this->request['reason_code'];
    }

    public function isFailedError(): bool
    {
        // todo
        return false;
    }
}
