<?php

namespace ITPolice\PaymentSystems\Systems\Best2Pay;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Interfaces\ServiceInterface;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;
use ITPolice\PaymentSystems\Systems\Best2Pay\Response;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;
use ITPolice\PaymentSystems\Traits\ServiceTrait;
use Illuminate\Http\Request;

class Service implements ServiceInterface
{
    use ServiceTrait;

    const MULTI_PAYMENT_PREFIX = 'multiPayment_';

    protected static $systemCode = 'best2pay';

    /**
     * Метод привязки карты
     * @return Response
     * @throws \Exception
     */
    public function cardBinding()
    {
        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона');
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email');
        }

        $reference = Carbon::now()->timestamp . '_' . $phone . '_' . rand(1000, 9999);

        $params = [
            'reference'   => $reference,
            'description' => 'Привязка карты',
            'phone'       => $phone,
            'email'       => $email,
            'url'         => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl,
            'amount'      => intval(env('PAY_SYSTEM_CARD_BINDING_AMOUNT', 1) * 100)
        ];
        $response = $this->register($params);

        if (!$response->success())
            return $response;

        $registerId = $response->id();

        $data = [
            'sector'    => $this->merchantId,
            'id'        => $registerId,
            'signature' => $this->signature($registerId)
        ];
        $link = $this->getBaseUrl() . '/webapi/CardEnroll?' . http_build_query($data);
        $response->setLink($link);
        return $response;
    }

    /**
     * Регистрация заказа
     * @param $data
     * @return Response
     * @throws \Exception
     */
    protected function register($data)
    {
        $data['currency'] = 643;
        $data['sector'] = $this->merchantId;
        $data['signature'] = $this->signature($data['amount'], $data['currency']);
        $data['life_period'] = 7200; // время жизни заказа в секундах
        return $this->transaction('/webapi/Register', $data);
    }

    /**
     * Генерация цифровой подписи
     * @param mixed ...$args
     * @return string
     */
    protected function signature(...$args)
    {
        $signature = $this->merchantId . implode($args) . $this->secret;
        return base64_encode(md5($signature));
    }

    /**
     * @param $method
     * @param $params
     * @return Response
     * @throws \Exception
     */
    protected function transaction($method, $params)
    {
        $baseUrl = $this->getBaseUrl();

        $params['mode'] = 0;
        $postParams = http_build_query(array_filter($params));

        $startTime = microtime(true);

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL            => $baseUrl . $method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 310,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSLVERSION     => 6,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => $postParams,
            CURLOPT_HTTPHEADER     => array(
                "Content-Type: application/x-www-form-urlencoded",
//                'Authorization: Basic ' . base64_encode($this->merchantId . ":" . $this->secret),
//                "Content-Type: application/json",
            ),
        ));
        $res = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $responseObj = null;
        try {
            $responseObj = json_decode(json_encode(simplexml_load_string($res), JSON_UNESCAPED_UNICODE));

            $requestExecutionTime = microtime(true) - $startTime;

            Log::debug('best2pay response '.$method, [
                'params'   => $params,
                'httpCode' => $httpCode,
                'response' => (array)$responseObj,
                'execTime' => $requestExecutionTime
            ]);
        } catch (\Throwable $e) {
            Log::error('best2pay transaction error', [
                'exception' => $e,
                'httpCode'  => $httpCode,
                'response'  => $res,
            ]);
        }

//        if (!@$responseObj->id) {
//            try {
//                Log::debug(__CLASS__ . ' ' . $params['payment']['action'] . ' transaction', [
//                    'params'   => $postParams,
//                    'response' => $res,
//                ]);
//            } catch (\Exception $e) {
//                //
//            }
//            throw new \Exception(@$responseObj->error);
//        }
        return new Response($responseObj);
    }

    /**
     * Получение базового URL
     * @return string
     */
    protected function getBaseUrl()
    {
        if (env('PAY_SYSTEM_TEST_MODE')) {
            $baseUrl = 'https://test.best2pay.net';
        } else {
            $baseUrl = 'https://pay.best2pay.net';
        }

        return $baseUrl;
    }

    /**
     * Выдача средств
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function payoutByToken($orderId, $amount, $cardToken, $description = 'Выдача')
    {
        $amount = intval($amount * 100);

        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $params = [
            'reference'   => $orderId,
            'description' => $description,
            'amount'      => $amount,
            'phone'       => $phone,
            'email'       => $email,
        ];
        $response = $this->register($params);

        if (!$response->success())
            return $response;

        $registerId = $response->id();

        $currency = 643;
        $signature = $this->signature($registerId, $amount, $currency, $cardToken);

        $params = [
            'sector'      => $this->merchantId,
            'amount'      => $amount,
            'currency'    => $currency,
            'token'       => $cardToken,
            'reference'   => $orderId,
            'description' => $description,
            'signature'   => $signature,
            'id'          => $registerId
        ];

        return $this->transaction('/gateweb/P2PCredit', $params);
    }

    /**
     * Оплата
     * @param $orderId
     * @param float $amount
     * @param string $description
     * @param float $fee
     * @return Response
     * @throws \Exception
     */
    public function pay($orderId, $amount, $description = '', $fee = 0)
    {
        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $amount = intval($amount * 100);
        $fee = intval(strval($fee * 100));

        $params = [
            'reference'   => $orderId,
            'description' => $description,
            'phone'       => $phone,
            'email'       => $email,
            'fee'         => $fee,
            'url'         => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl,
            'amount'      => $amount
        ];

        if (!empty($this->multiPayments)) {
            // Значение для передачи данных обо всех дополнительных платежах
            $multipaymentData = [
                'payments' => []
            ];

            foreach ($this->multiPayments as $multiPayment) {
                $multipaymentData['payments'][] = [
                    'amount'      => intval($multiPayment->getAmount() * 100),
                    'fee'         => intval($multiPayment->getFee() * 100),
                    'currency'    => 643,
                    'description' => $multiPayment->getDescription(),
                    'paymentNum'  => intval($multiPayment->getMerchant()),
                    'reference'   => self::MULTI_PAYMENT_PREFIX . $multiPayment->getReference()
                ];
            }

            $params['multipaymentData'] = json_encode($multipaymentData, JSON_UNESCAPED_UNICODE);
        }

        $response = $this->register($params);

        if (!$response->success())
            return $response;

        $registerId = $response->id();

        $data = [
            'sector'    => $this->merchantId,
            'id'        => $registerId,
            'signature' => $this->signature($registerId)
        ];
        $link = $this->getBaseUrl() . '/webapi/Purchase?' . http_build_query($data);
        $response->setLink($link);
        return $response;
    }

    /**
     * todo
     * Повторное списание средств на основе ID обычной оплаты
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return \ITPolice\PaymentSystems\Systems\MandarinBank\Response
     * @throws \Exception
     */
    public function recurring($orderId, $amount, $cardToken, $description = '')
    {
        // todo
    }

    /**
     * Безакцептное списание средств по токену
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function recurringByToken($orderId, $amount, $cardToken, $description = 'Регулярный платеж')
    {
        $amount = intval($amount * 100);

        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $params = [
            'reference'   => $orderId,
            'description' => $description,
            'phone'       => $phone,
            'email'       => $email,
            // 'fee' => 0, // todo
            'amount'      => $amount
        ];
        $response = $this->register($params);

        if (!$response->success())
            return $response;

        $registerId = $response->id();

        $params = [
            'sector'    => $this->merchantId,
            'id'        => $registerId,
            'token'     => $cardToken,
            'signature' => $this->signature($registerId, $cardToken)
        ];

        return $this->transaction('/webapi/PurchaseByToken', $params);
    }

    public function setCallbackRequest(Request $request, $POST = null, $GET = null)
    {
        $data = [];
        foreach ((array)simplexml_load_string($request->getContent()) as $key => $value) {
            if ($value instanceof \SimpleXMLElement) {
                $value = $value->__toString();
            }
            $data[$key] = $value;
        }
        
        foreach ($data as $k => $v) {
            if (is_array($v) && empty($v)) unset($data[$k]);
        }
        $this->callbackRequest = new CallbackRequest($data);
        $this->callbackRequest->setMerchant($this->merchantId);
        $this->callbackRequest->setSecret($this->secret);
        return $this;
    }

    public function responseCallback(): string
    {
        return 'ok';
    }

    public function callbackAfterSetOperation(PaymentSystemTransaction $previousModel)
    {

    }

    public function hasMultiPayments(): bool
    {
        return true;
    }

    public function getCardsPayIn(): array
    {
        throw new \Exception('Method not available');
    }

    public function getCardsPayOut(): array
    {
        throw new \Exception('Method not available');
    }

    public function reverseCardBinding(PaymentSystemTransaction $model)
    {
        try {
            $amount   = intval(env('PAY_SYSTEM_CARD_BINDING_AMOUNT', 1) * 100);
            $currency = 643;

            /** Дополнительно проверяем, что в транзакции была привязка карты */
            if ($model->operation == 'cardBinding') {
                $params = [
                    'sector'    => $this->merchantId,
                    'id'        => $model->transaction_id,
                    'amount'    => $amount,
                    'currency'  => $currency,
                    'signature' => $this->signature($model->transaction_id, $amount, $currency)
                ];

                $this->transaction('/webapi/Reverse', $params);
            }
        } catch (\Throwable $e) {
            Log::error('best2pay reverse card binding error', [
                'exception' => $e
            ]);
        }
    }

    public function orderInfo(string $orderId)
    {
        $params = [
            'sector'    => $this->merchantId,
            'id'        => $orderId,
            'signature' => $this->signature($orderId)
        ];

        return $this->transaction('/webapi/Order', $params);
    }
}
