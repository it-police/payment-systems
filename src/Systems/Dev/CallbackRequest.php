<?php

namespace ITPolice\PaymentSystems\Systems\Dev;

use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;

class CallbackRequest implements CallbackRequestInterface
{
    use CallbackRequestTrait;

    /**
     * Метод проверки подписи
     * @param array $request
     * @return bool
     */
    public function checkSign()
    {
        return true;
    }

    public function transactionId()
    {
        $transactionId = @$this->request['id'];
        return $transactionId;
    }

    public function action()
    {
        $method = @$this->request['method'];
        if ($method == 'pay' || $method == 'recurringByToken') {
            return self::ACTION_PAY;
        } elseif ($method == 'payoutByToken') {
            return self::ACTION_PAYOUT;
        } elseif ($method == 'cardBinding') {
            return self::ACTION_CARD_BINDING;
        }
        return null;
    }

    public function errorMessage(): string
    {
        return (string) @$this->request['message'];
    }

    public function amount(): float
    {
        return (float)@$this->request['amount'];
    }

    public function bankCard(): BankCard
    {
        $bankCard = new BankCard();

        $token = $this->request['card']['token'];
        $cardBin = $this->request['card']['bin'];
        $cardPan = $this->request['card']['pan'];
        $cardMonth = $this->request['card']['month'];
        $cardYear = $this->request['card']['year'];
        $cardHolder = $this->request['card']['holder'];

        $bankCard->token = $token;
        $bankCard->bin = $cardBin;
        $bankCard->pan = $cardPan;
        $bankCard->month = $cardMonth;
        $bankCard->year = $cardYear;
        $bankCard->holder = $cardHolder;

        return $bankCard;
    }

    public function isStopError(): bool
    {
        switch ((int)$this->errorCode()) {
            case 2:
            case 3:
                return true;
        }
        return false;
    }

    public function status()
    {
        $status = @$this->request['status'];
        switch ($status) {
            case 'success':
                return self::STATUS_SUCCESS;
            case 'error':
                return self::STATUS_ERROR;
            default:
                return $status;
        }
    }

    public function errorCode()
    {
        return @$this->request['reason_code'];
    }

    public function isFailedError(): bool
    {
        // todo
        return false;
    }
}
