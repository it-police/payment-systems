<?php

namespace ITPolice\PaymentSystems\Systems\Dev;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Interfaces\ServiceInterface;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;
use ITPolice\PaymentSystems\Systems\Dev\CallbackRequest;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;
use ITPolice\PaymentSystems\Traits\ServiceTrait;
use Illuminate\Http\Request;

class Service implements ServiceInterface
{
    use ServiceTrait;

    protected static $systemCode = 'dev';

    /**
     * Метод привязки карты
     * @return Response
     * @throws \Exception
     */
    public function cardBinding()
    {
        return $this->makeResponse(__METHOD__, 1, 'Привязка карты');
    }

    protected function makeResponse($method, $amount, $description = null, $token = null, $orderId = null) {
        $id = uniqid();
        $callbackUrl = $this->callbackUrl;
        $returnUrl = $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl;
        $operation = $this->operation;
        $method = str_replace(__CLASS__.'::', '', $method);
        $phone = $this->preparePhone($this->phone);

        $params = [
            'id'          => $id,
            'returnUrl'   => $returnUrl,
            'callbackUrl' => $callbackUrl,
            'description' => $description,
            'phone'       => $phone,
            'amount'      => $amount,
            'operation'   => $operation,
            'method'      => $method,
            'cardToken'      => $token,
            'orderId' => $orderId
        ];

        $responseObj = (object)[
            'id'   => $params['id'],
            'link' => route('pay-system.dev.transaction', $params),
            'params'   => $params,
        ];

        return new Response($responseObj);
    }

    /**
     * Выдача средств
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function payoutByToken($orderId, $amount, $cardToken, $description = 'Выдача')
    {
        $response = $this->makeResponse(__METHOD__, $amount, $description, $cardToken, $orderId);
        $this->sendCurl($response->getResponse()->params);
        return $response;
    }

    public function sendCurl($postParams) {
        $operation = $postParams['operation'];
        $callbackUrl = $postParams['callbackUrl'];
        $returnUrl = $postParams['returnUrl'];

        $postParams['status'] = true;

        if ($operation == 'cardBinding') {
            $postParams['card'] = [
                'token'  => Str::uuid()->toString(),
                'bin'    => rand(100000, 999999),
                'pan'    => rand(1000, 9999),
                'month'  => 12,
                'year'   =>  (int) substr(date('Y', strtotime('+1 year')), 2),
                'holder' => 'CARD HOLDER',
            ];
        }

        $postParamsJson = json_encode($postParams, JSON_UNESCAPED_UNICODE);

        $cmd = "curl -X POST {$callbackUrl} -H 'Content-Type: application/json' -d '{$postParamsJson}'";

        shell_exec('(sleep 5 && ' . $cmd . ') > /dev/null 2>&1 &');

        Log::debug(__CLASS__, [
            'callbackUrl' => $callbackUrl,
            'postParams'  => $postParams
        ]);
    }

    /**
     * Оплата
     * @param $orderId
     * @param float $amount
     * @param string $description
     * @param float $fee
     * @return Response
     * @throws \Exception
     */
    public function pay($orderId, $amount, $description = '', $fee = 0)
    {
        return $this->makeResponse(__METHOD__, $amount, $description, null, $orderId);
    }

    /**
     * todo
     * Повторное списание средств на основе ID обычной оплаты
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return \ITPolice\PaymentSystems\Traits\ResponseTrait
     * @throws \Exception
     */
    public function recurring($orderId, $amount, $cardToken, $description = '')
    {
        // todo
    }

    /**
     * Безакцептное списание средств по токену
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function recurringByToken($orderId, $amount, $cardToken, $description = 'Регулярный платеж')
    {
        $response = $this->makeResponse(__METHOD__, $amount, $description, $cardToken, $orderId);
        $this->sendCurl($response->getResponse()->params);
        return $response;
    }

    public function setCallbackRequest(Request $request, $POST = null, $GET = null)
    {
        $data = $request->all();
        $this->callbackRequest = new CallbackRequest($data);
        $this->callbackRequest->setPost($POST);
        $this->callbackRequest->setMerchant($this->merchantId);
        $this->callbackRequest->setSecret($this->secret);
        return $this;
    }

    public function responseCallback(): string
    {
        return 'ok';
    }

    public function callbackAfterSetOperation(PaymentSystemTransaction $previousModel)
    {

    }

    public function hasMultiPayments(): bool
    {
        return false;
    }

    public function getCardsPayIn(): array
    {
        throw new \Exception('Method not available');
    }

    public function getCardsPayOut(): array
    {
        throw new \Exception('Method not available');
    }

    public function reverseCardBinding(PaymentSystemTransaction $model)
    {
        // TODO: Implement reverseCardBinding() method.
    }

    public function orderInfo(string $orderId)
    {
        // TODO: Implement orderInfo() method.
    }
}
