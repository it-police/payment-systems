<?php

namespace ITPolice\PaymentSystems\Systems\Dev;


use ITPolice\PaymentSystems\Interfaces\ResponseInterface;
use ITPolice\PaymentSystems\Traits\ResponseTrait;

class Response implements ResponseInterface
{
    use ResponseTrait;

    public function parse($response)
    {
        $this->id = @$response->id;
        $this->success = true;
        $this->link = $response->link;
    }
}
