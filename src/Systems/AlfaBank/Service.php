<?php

namespace ITPolice\PaymentSystems\Systems\AlfaBank;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Interfaces\ServiceInterface;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;
use ITPolice\PaymentSystems\Traits\ServiceTrait;

class Service implements ServiceInterface
{
    use ServiceTrait;

    protected static $systemCode = 'alfabank';

    /**
     * Метод привязки карты
     * @return Response
     * @throws \Exception
     */
    public function cardBinding()
    {
        $phone = $this->preparePhone($this->phone);
        if ( ! $phone) {
            throw new \Exception('Не указан номер телефона');
        }

        $email = (string)$this->email;
        if ( ! $email) {
            $email = $this->defaultEmail;
        }

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email');
        }

        $reference = Carbon::now()->timestamp.'_'.$phone.'_'.rand(1000, 9999);

        $params = [
            'orderNumber' => $reference,
            'amount'      => env('PAY_SYSTEM_CARD_BINDING_AMOUNT', 1),
            'comment'     => 'Привязка карты',
            'returnUrl'   => $this->returnUrl ?: $this->defaultReturnUrl,
            'clientId'    => $this->userId,
            'jsonParams'  => [
                'phone'  => $phone,
                'action' => CallbackRequestInterface::ACTION_CARD_BINDING,
            ],
            //'features' => 'FORCE_CREATE_BINDING'
        ];

        $response = $this->register($params);

        if ( ! $response->success()) {
            return $response;
        }

        $response->setLink($response->link());

        return $response;
    }

    /**
     * Регистрация заказа
     *
     * @param $data
     *
     * @return Response
     * @throws \Exception
     */
    protected function register($data)
    {
        return $this->transaction('register.do', $data);
    }


    /**
     * @param $request
     * @param $params
     *
     * @return Response
     * @throws \Exception
     */
    protected function transaction($request, $params, string $method = 'POST')
    {
        $url                = $this->getBaseUrl().$request;
        $params['userName'] = $this->merchantId;
        $params['password'] = $this->secret;

        if (isset($params['amount'])) {
            $params['amount'] = intval($params['amount'] * 100); // приводим к копейкам
        }

        if (isset($params['jsonParams'])) {
            $params['jsonParams'] = json_encode($params['jsonParams']);
        }

        $startTime = microtime(true);

        $ch = curl_init();

        if (strtolower($method) !== 'get') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        } else {
            $url .= '?'.http_build_query($params);
        }

        curl_setopt_array($ch, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 310,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSLVERSION     => 6,
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_HTTPHEADER     => array(
                "Accept: application/json",
            ),
        ));
        $res      = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $responseObj = null;
        try {
            $responseObj = json_decode($res);

            $requestExecutionTime = microtime(true) - $startTime;

            Log::debug('AlfaBank response '.$request, [
                'params'   => $params,
                'httpCode' => $httpCode,
                'response' => (array)$responseObj,
                'execTime' => $requestExecutionTime
            ]);
        } catch (\Throwable $e) {
            Log::error('AlfaBank transaction error', [
                'exception' => $e,
                'httpCode'  => $httpCode,
                'response'  => $res,
            ]);
        }

        return new Response($responseObj);
    }

    /**
     * Получение базового URL
     * @return string
     */
    protected function getBaseUrl()
    {
        if (env('PAY_SYSTEM_TEST_MODE')) {
            $baseUrl = 'https://alfa.rbsuat.com/payment/rest/';
        } else {
            $baseUrl = 'https://pay.alfabank.ru/payment/rest/';
        }

        return $baseUrl;
    }

    /**
     * Выдача средств
     *
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     *
     * @return Response
     * @throws \Exception
     */
    public function payoutByToken($orderId, $amount, $cardToken, $description = 'Выдача')
    {
        throw new \Exception('Method not available');
    }

    /**
     * Оплата
     *
     * @param $orderId
     * @param float $amount
     * @param string $description
     * @param float $fee
     *
     * @return Response
     * @throws \Exception
     */
    public function pay($orderId, $amount, $description = '', $fee = 0)
    {
        $phone = $this->preparePhone($this->phone);
        if ( ! $phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if ( ! $email) {
            $email = $this->defaultEmail;
        }

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $amount    = ceil(($amount + $fee) * 100) / 100;
        $reference = Carbon::now()->timestamp.'_'.$phone.'_'.rand(1000, 9999);

        $params = [
            'orderNumber' => $reference,
            'amount'      => round($amount, 2),
            'comment'     => $description,
            'returnUrl'   => $this->returnUrl ?: $this->defaultReturnUrl,
            'clientId'    => $this->userId,
            'jsonParams'  => [
                'phone' => $phone,
            ],
        ];

        $response = $this->register($params);

        if ( ! $response->success()) {
            return $response;
        }

        $response->setLink($response->link());

        return $response;
    }

    /**
     * todo
     * Повторное списание средств на основе ID обычной оплаты
     *
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     *
     * @return \ITPolice\PaymentSystems\Systems\MandarinBank\Response
     * @throws \Exception
     */
    public function recurring($orderId, $amount, $cardToken, $description = '')
    {
        throw new \Exception('Method not available');
    }

    /**
     * Безакцептное списание средств по токену
     *
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     *
     * @return Response
     * @throws \Exception
     */
    public function recurringByToken($orderId, $amount, $cardToken, $description = 'Регулярный платеж')
    {
        $phone = $this->preparePhone($this->phone);
        if ( ! $phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if ( ! $email) {
            $email = $this->defaultEmail;
        }

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $reference = Carbon::now()->timestamp.'_'.$phone.'_'.rand(1000, 9999);

        $params = [
            'amount'      => round($amount, 2),
            'description' => $description,
            'orderNumber' => $reference,
            'clientId'    => $this->userId,
            'returnUrl'   => $this->returnUrl ?: $this->defaultReturnUrl,
            'jsonParams'  => [
                'action' => 'recurringByToken',
                'phone'  => $phone,
            ],
            'features'    => 'AUTO_PAYMENT',
        ];

        $response = $this->transaction('register.do', $params);

        /** Если платеж успешно создан - оплачиваем его по токену */
        if ($response->success() && $response->id()) {
            $params = [
                'mdOrder'   => $response->id(),
                'bindingId' => $cardToken,
            ];

            $this->transaction('paymentOrderBinding.do', $params);
        }

        return $response;
    }

    public function setCallbackRequest(Request $request, $POST = null, $GET = null)
    {
        $order = new Response([]);
        if ( ! empty($request->get('mdOrder')) && ! empty($request->get('operation'))) {
            if (in_array($request->get('operation'), ['deposited', 'bindingCreated'])) {
                $order = $this->orderInfo($request->get('mdOrder'));
            }
        }

        $this->callbackRequest = new CallbackRequest(array_merge($request->all(), ['order' => $order->getResponse()]));
        $this->callbackRequest->setMerchant($this->merchantId);
        $this->callbackRequest->setSecret($this->secret);


        return $this;
    }

    public function responseCallback(): string
    {
        return 'ok';
    }

    public function callbackAfterSetOperation(PaymentSystemTransaction $previousModel)
    {
    }

    public function hasMultiPayments(): bool
    {
        return false;
    }

    public function getCardsPayIn(): array
    {
        throw new \Exception('Method not available');
    }

    public function getCardsPayOut(): array
    {
        throw new \Exception('Method not available');
    }

    public function reverseCardBinding(PaymentSystemTransaction $model)
    {
        // TODO: Implement reverseCardBinding() method.
    }

    public function orderInfo(string $orderId)
    {
        return $this->transaction('getOrderStatusExtended.do', ['orderId' => $orderId]);
    }
}
