<?php

namespace ITPolice\PaymentSystems\Systems\AlfaBank;

use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;

class CallbackRequest implements CallbackRequestInterface
{
    use CallbackRequestTrait;

    /**
     * Метод проверки подписи
     *
     * @return bool
     */
    public function checkSign()
    {
        $request = $this->request;
        $sign    = $request['checksum'] ?? $request['sign_alias'] ?? null;
        unset($request['checksum'], $request['sign_alias'], $request['order']);

        if (empty($sign)) {
            return true;
        }
        
        ksort($request);
        $signature = '';
        foreach ($request as $key => $value) {
            $signature .= sprintf('%s;%s;', $key, $value);
        }

        $signature = strtoupper(hash_hmac('sha256', $signature, $this->callbackSecret));

        return $signature === $sign;
    }

    public function transactionId()
    {
        return @$this->request['mdOrder'];
    }

    public function action()
    {
        $operation = @$this->request['operation'];

        return match ($operation) {
            'bindingCreated' => self::ACTION_CARD_BINDING,
            'approved', 'deposited' => self::ACTION_PAY,
            'refunded', 'reversed' => self::ACTION_REVERSE,
            default => null,
        };
    }

    public function errorMessage(): string
    {
        return (string)@$this->request['errorMessage'];
    }

    public function amount(): float
    {
        return (float)@$this->request['amount'] / 100;
    }

    public function bankCard(): BankCard
    {
        $bankCard = new BankCard();

        $token          = @$this->request['bindingId'];
        $cardBin        = substr(@$this->request['maskedPan'], 0, 6);
        $cardPan        = substr(@$this->request['maskedPan'], -4);
        $cardholderName = 'UNKNOWN NAME';
        $cardYear       = null;
        $cardMonth      = null;

        if ( ! empty($this->request['order']->cardAuthInfo)) {
            $cardBin        = substr(@$this->request['order']->cardAuthInfo->maskedPan, 0, 6);
            $cardPan        = substr(@$this->request['order']->cardAuthInfo->maskedPan, -4);
            $cardYear       = substr(@$this->request['order']->cardAuthInfo->expiration, 2, 2);
            $cardMonth      = substr(@$this->request['order']->cardAuthInfo->expiration, 4);
            $cardholderName = @$this->request['order']->cardAuthInfo->cardholderName;
        }

        if ( ! empty($this->request['order']->bindingInfo->bindingId)) {
            $token = $this->request['order']->bindingInfo->bindingId;
        }

        $bankCard->token    = $token;
        $bankCard->bin      = $cardBin;
        $bankCard->pan      = $cardPan;
        $bankCard->month    = $cardMonth;
        $bankCard->year     = $cardYear;
        $bankCard->holder   = $cardholderName;
        $bankCard->bankName = null;

        return $bankCard;
    }

    public function isStopError(): bool
    {
        if ((int)$this->errorCode() > 0) {
            return true;
        }

        return false;
    }

    public function status()
    {
        $operation = @$this->request['operation'];
        $status    = @$this->request['status'];
        $order     = @$this->request['order'];

        if ( ! empty($order)) {
            if ((int)@$order->errorCode > 0) {
                $this->request['errorCode']    = $order->errorCode;
                $this->request['errorMessage'] = @$order->errorMessage;

                return self::STATUS_ERROR;
            } elseif ((int)@$order->actionCode > 0) {
                $this->request['errorCode']    = $order->actionCode;
                $this->request['errorMessage'] = @$order->actionCodeDescription;

                return self::STATUS_ERROR;
            }

            return match ((int)@$order->orderStatus) {
                1, 2 => self::STATUS_SUCCESS,
                3, 4, 6 => self::STATUS_ERROR,
                default => self::STATUS_NEW,
            };
        }

        return match ($operation) {
            'declinedByTimeout' => self::STATUS_ERROR,
            default => $status,
        };
    }

    public function errorCode()
    {
        return @$this->request['errorCode'];
    }

    public function isFailedError(): bool
    {
        return false;
    }
}
