<?php

namespace ITPolice\PaymentSystems\Systems\AlfaBank;

use ITPolice\PaymentSystems\Interfaces\ResponseInterface;
use ITPolice\PaymentSystems\Traits\ResponseTrait;

class Response implements ResponseInterface
{
    use ResponseTrait;

    public function parse($response)
    {
        if ( ! empty($response->errorCode)) {
            $this->errorMessage = (string)(@$response->message ?? @$response->errorCode);
        } else {
            $this->success = true;
            $this->id      = @$response->orderId;
            $this->link    = $response->formUrl ?? null;
        }
    }
}
