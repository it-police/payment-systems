<?php

namespace ITPolice\PaymentSystems\Systems\MandarinBank;

use Illuminate\Support\Facades\Log;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Interfaces\ServiceInterface;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;
use ITPolice\PaymentSystems\Traits\ServiceTrait;
use Illuminate\Http\Request;

class Service implements ServiceInterface
{
    use ServiceTrait;

    protected static $systemCode = 'mandarin';

    /**
     * Метод привязки карты
     * @return Response
     * @throws \Exception
     */
    public function cardBinding()
    {
        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона');
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email');
        }

        $params = [
            'customerInfo' => [
                "email" => $email,
                "phone" => "+$phone"
            ],
            'urls'         => [
                "callback" => $this->callbackUrl,
                "return"   => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl
            ],
        ];

        return $this->transaction($params, 'card-bindings');
    }

    /**
     * @param $params
     * @param $method
     * @return Response
     * @throws \Exception
     */
    protected function transaction($params, $method = 'transactions')
    {
        $postParams = json_encode($params, JSON_UNESCAPED_UNICODE);

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL            => "https://secure.mandarinpay.com/api/".$method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSLVERSION     => 6,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => $postParams,
            CURLOPT_HTTPHEADER     => array(
                'Authorization: Basic ' . base64_encode($this->merchantId . ":" . $this->secret),
                "Content-Type: application/json",
            ),
        ));
        $res = curl_exec($ch);
        curl_close($ch);
        $responseObj = json_decode($res);
        if (!@$responseObj->id) {
            try {
                Log::debug(__CLASS__ . ' ' . $params['payment']['action'] . ' transaction', [
                    'params'   => $postParams,
                    'response' => $res,
                ]);
            } catch (\Exception $e) {
                //
            }
            throw new \Exception(@$responseObj->error);
        }
        return new Response($responseObj);
    }

    /**
     * Выдача средств
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function payoutByToken($orderId, $amount, $cardToken, $description = '')
    {
        $customValues = [];
        if ($description) {
            $customValues[] = [
                'name'  => 'description',
                'value' => $description
            ];
        }
        $params = [
            'payment'      => [
                "orderId" => $orderId,
                "action"  => 'payout',
                "price"   => $amount
            ],
            "customValues" => $customValues,
            'urls'         => [
                "callback" => $this->callbackUrl,
                "return"   => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl
            ],
            'target'       => [
                "card" => $cardToken
            ]
        ];

        return $this->transaction($params);
    }

    /**
     * Оплата
     * @param $orderId
     * @param float $amount
     * @param string $description
     * @param float $fee
     * @return Response
     * @throws \Exception
     */
    public function pay($orderId, $amount, $description = '', $fee = 0)
    {
        $amount = $amount + $fee;
        $amount = ceil($amount * 100) / 100;

        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $customValues = [];
        if ($description) {
            $customValues[] = [
                'name'  => 'description',
                'value' => $description
            ];
        }

        $params = [
            'payment'      => [
                "orderId" => $orderId,
                "action"  => 'pay',
                "price"   => $amount
            ],
            "customerInfo" => [
                "email" => $email,
                "phone" => "+$phone"
            ],
            "customValues" => $customValues,
            'urls'         => [
                "callback" => $this->callbackUrl,
                "return"   => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl
            ]
        ];

        return $this->transaction($params);
    }

    /**
     * todo
     * Повторное списание средств на основе ID обычной оплаты
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return \ITPolice\PaymentSystems\Systems\MandarinBank\Response
     * @throws \Exception
     */
    public function recurring($orderId, $amount, $cardToken, $description = '') {
        // todo
    }

    /**
     * Безакцептное списание средств по токену
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function recurringByToken($orderId, $amount, $cardToken, $description = '')
    {
        $customValues = [];
        if ($description) {
            $customValues[] = [
                'name'  => 'description',
                'value' => $description
            ];
        }
        $params = [
            'payment'      => [
                "orderId" => $orderId,
                "action"  => 'pay',
                "price"   => $amount
            ],
            "customValues" => $customValues,
            'target'       => [
                "card" => $cardToken
            ],
            'urls'         => [
                "callback" => $this->callbackUrl
            ]
        ];

        return $this->transaction($params);
    }

    public function setCallbackRequest(Request $request, $POST = null, $GET = null)
    {
        $data = $POST;
        $this->callbackRequest = new CallbackRequest($data);
        $this->callbackRequest->setPost($POST);
        $this->callbackRequest->setMerchant($this->merchantId);
        $this->callbackRequest->setSecret($this->secret);
        return $this;
    }

    public function responseCallback(): string
    {
        return 'OK';
    }

    public function callbackAfterSetOperation(PaymentSystemTransaction $previousModel)
    {
        if($previousModel) {
            $this->setMerchant($previousModel->merchant);
        }
    }

    public function hasMultiPayments(): bool
    {
        /**
         * todo
         * Проверить возможность использования
         * и при необходимости дополнить параметры в методе pay()
         */
        return false;
    }

    public function getCardsPayIn(): array
    {
        throw new \Exception('Method not available');
    }

    public function getCardsPayOut(): array
    {
        throw new \Exception('Method not available');
    }

    public function reverseCardBinding(PaymentSystemTransaction $model)
    {
        // TODO: Implement reverseCardBinding() method.
    }

    public function orderInfo(string $orderId)
    {
        // TODO: Implement orderInfo() method.
    }
}
