<?php

namespace ITPolice\PaymentSystems\Systems\MandarinBank;

use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;

class CallbackRequest implements CallbackRequestInterface
{
    use CallbackRequestTrait;

    /**
     * Метод проверки подписи
     * @param array $request
     * @return bool
     */
    public function checkSignTest()
    {
        $request = $_POST;
        $secret = $this->secret;

        $sign = $request['sign'];
        unset($request['sign']);
        $to_hash = '';
        if (!is_null($request) && is_array($request)) {
            ksort($request);
            $to_hash = implode('-', $request);
        }

        $to_hash = $to_hash . '-' . $secret;
        $calculated_sign = hash('sha256', $to_hash);
        return $calculated_sign == $sign;
    }

    /**
     * Метод проверки подписи
     * @param array $request
     * @return bool
     */
    public function checkSign()
    {
        $request = $_POST;
        $secret = $this->secret;

        $sign = $request['sign'];
        unset($request['sign']);
        $to_hash = '';
        if (!is_null($request) && is_array($request)) {
            ksort($request);
            $to_hash = implode('-', $request);
        }

        $to_hash = $to_hash . '-' . $secret;
        $calculated_sign = hash('sha256', $to_hash);

        return true; // todo $calculated_sign == $sign;
    }

    public function transactionId()
    {
        $transactionId = @$this->request['transaction'];
        if (!$transactionId) {
            $transactionId = @$this->request['card_binding'];
        }
        return $transactionId;
    }

    public function action()
    {
        $action = @$this->request['action'];
        $object_type = @$this->request['object_type'];

        if ($action == 'pay' && $object_type == 'transaction') {
            return self::ACTION_PAY;
        } elseif ($action == 'payout' && $object_type == 'transaction') {
            return self::ACTION_PAYOUT;
        } elseif ($object_type == 'card_binding') {
            return self::ACTION_CARD_BINDING;
        }
        return null;
    }

    public function errorMessage(): string
    {
        return (string)@$this->request['error_description'];
    }

    public function amount(): float
    {
        return (float)@$this->request['price'];
    }

    public function bankCard(): BankCard
    {
        $bankCard = new BankCard();

        $token = $this->request['card_binding'];
        $cardBin = mb_substr($this->request['card_number'], 0, 6);
        $cardPan = mb_substr($this->request['card_number'], -4);
        $cardMonth = str_pad($this->request['card_expiration_month'], 2, '0', STR_PAD_LEFT);
        $cardYear = 2000 + (int)$this->request['card_expiration_year'];
        $cardHolder = $this->request['card_holder'];

        $bankCard->token = $token;
        $bankCard->bin = $cardBin;
        $bankCard->pan = $cardPan;
        $bankCard->month = $cardMonth;
        $bankCard->year = $cardYear;
        $bankCard->holder = $cardHolder;

        return $bankCard;
    }

    public function isStopError(): bool
    {
        if (@$this->request['status'] === 'payout-only') {
            return true;
        }

        switch ((int)$this->errorCode()) {
            case 4:
            case 7:
            case 14:
            case 33:
            case 35:
            case 36:
            case 37:
            case 41:
            case 43:
            case 54:
            case 57:
            case 58:
            case 66:
                return true;
        }
        return false;
    }

    public function status()
    {
        $status = @$this->request['status'];
        switch ($status) {
            case 'success':
                return self::STATUS_SUCCESS;
            case 'failed':
            case 'payout-only':
                return self::STATUS_ERROR;
            default:
                return $status;
        }
    }

    public function errorCode()
    {
        return @$this->request['error_code'];
    }

    public function isFailedError(): bool
    {
        switch ((int)$this->errorCode()) {
            case -1:
            case 5:
            case 51:
            case 61:
            case 65:
            case 1123:
                return true;
        }
        return false;
    }
}
