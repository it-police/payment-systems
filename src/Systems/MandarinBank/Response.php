<?php

namespace ITPolice\PaymentSystems\Systems\MandarinBank;


use ITPolice\PaymentSystems\Interfaces\ResponseInterface;
use ITPolice\PaymentSystems\Traits\ResponseTrait;

class Response implements ResponseInterface
{
    use ResponseTrait;

    public function parse($response)
    {
        $this->id = @$response->id;
        $this->link = @$response->userWebLink;
        $this->success = (bool) @$response->id;
        $this->errorMessage = (string) @$response->error;
    }
}
