<?php

namespace ITPolice\PaymentSystems\Systems\ImPay;

use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;
use ITPolice\PaymentSystems\Traits\ServiceTrait;

class CallbackRequest implements CallbackRequestInterface
{
    use CallbackRequestTrait;

    /**
     * Метод проверки подписи
     * @param array $request
     * @return bool
     */
    public function checkSignTest()
    {
        $request = $this->get;
        $secret = env('PAY_SYSTEM_SECRET_KEY_impay');
        $sign = $request['key'];
        unset($request['key']);
        $check = md5(implode('', $request)  . $secret);
        return $sign == $check;
    }

    /**
     * Метод проверки подписи
     * @param array $request
     * @return bool
     */
    public function checkSign()
    {
        $request = $this->get;
        $secret = env('PAY_SYSTEM_SECRET_KEY_impay');
        $sign = $request['key'];
        unset($request['key']);
        $check = md5(implode('', $request)  . $secret);
        return $sign == $check;
    }

    public function transactionId()
    {
        return @$this->request['id'];
    }

    public function action()
    {
        return null;
    }

    public function isActionPay()
    {
        return true;
    }

    public function isActionPayout()
    {
        return true;
    }

    public function isActionCardBinding()
    {
        return true;
    }

    public function errorMessage(): string
    {
        return (string)@$this->request['message'];
    }

    public function amount(): float
    {
        return (float)@$this->request['sum'];
    }

    public function bankCard(): BankCard
    {
        $bankCard = new BankCard();

        $impayService = new Service();
        $card = $impayService->getCard($this->request['id']);

        $token = $card['card']->id;
        $cardBin = mb_substr($card['card']->num, 0, 6);
        $cardPan = mb_substr($card['card']->num, -4);
        $carddate = explode('/', $card['card']->exp);
        $cardMonth = str_pad($carddate[0], 2, '0', STR_PAD_LEFT);
        $cardYear = 2000 + (int)$carddate[1];
        $cardHolder = $card['card']->holder;

        $bankCard->token = $token;
        $bankCard->bin = $cardBin;
        $bankCard->pan = $cardPan;
        $bankCard->month = $cardMonth;
        $bankCard->year = $cardYear;
        $bankCard->holder = $cardHolder;

        return $bankCard;
    }

    public function isStopError(): bool
    {
        if (@$this->request['status'] === 'payout-only') {
            return true;
        }

        switch ((int)$this->errorCode()) {
            case 0:
            case 2:
            case 3:
                return true;
        }
        return false;
    }

    public function errorCode()
    {
        return @$this->request['status'];
    }

    public function status()
    {
        $status = @$this->request['status'];
        switch ($status) {
            case 1:
                return self::STATUS_SUCCESS;
            case 0:
            case 2:
            case 3:
                return self::STATUS_ERROR;
            default:
                return $status;
        }
    }

    public function isFailedError(): bool
    {
        switch ((int)$this->errorCode()) {
            case 0:
            case 2:
            case 3:
                return true;
        }
        return false;
    }
}
