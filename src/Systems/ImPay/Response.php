<?php

namespace ITPolice\PaymentSystems\Systems\ImPay;


use ITPolice\PaymentSystems\Interfaces\ResponseInterface;
use ITPolice\PaymentSystems\Traits\ResponseTrait;

class Response implements ResponseInterface
{
    use ResponseTrait;

    public function parse($response)
    {
        $this->id = @$response->id;
        $this->link = @$response->url;
        $this->success = (bool) @$response->status;
        $this->errorMessage = (string) @$response->message;
    }
}
