<?php

namespace ITPolice\PaymentSystems\Systems\ImPay;

use Illuminate\Support\Facades\Log;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Interfaces\ServiceInterface;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;
use ITPolice\PaymentSystems\Traits\ServiceTrait;
use Illuminate\Http\Request;
use Carbon\Carbon;
use ITPolice\PaymentSystems\Systems\ImPay\Response;

class Service implements ServiceInterface
{
    use ServiceTrait;

    protected static $systemCode = 'impay';

    /**
     * Метод привязки карты
     * @return Response
     * @throws \Exception
     */
    public function cardBinding()
    {
        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона');
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email');
        }

        $reference = Carbon::now()->timestamp . '_' . $phone . '_' . rand(1000, 9999);

        $params = [
            'type' => 0,
            'extid' => $reference,
            'timeout' => 10,
            'successurl' => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl,
            'failurl' => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl,
            'cancelurl' => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl
        ];

        return $this->transaction($params, 'card/reg', "POST");
    }

    /**
     * @param $params
     * @param $request
     * @param $method
     * @return Response
     * @throws \Exception
     */
    public function transaction($params, $request = 'transactions', $method = 'GET')
    {
        $postParams = json_encode($params, JSON_UNESCAPED_UNICODE);
        $baseUrl = $this->getBaseUrl();
        $system = self::$systemCode;
        $merchantId = env("PAY_SYSTEM_MERCHANT_{$system}");

        $key = env("PAY_SYSTEM_SECRET_{$system}_{$merchantId}");
        $token = sha1(sha1($key) . sha1($postParams));

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL            => $baseUrl.$request,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSLVERSION     => 6,
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_POSTFIELDS     => $postParams,
            CURLOPT_HTTPHEADER     => array(
                'X-Login: ' . $merchantId,
                'X-Token: '. $token,
                "Content-Type: application/json",
            ),
        ));
        $res = curl_exec($ch);
        curl_close($ch);
        $responseObj = json_decode($res);
        Log::debug('impay params ' . $request . " " .$method, $params);
        Log::debug('impay response ' . $request, (array)$responseObj);

        return new Response($responseObj);
    }

    /**
     * Выдача средств
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function payoutByToken($orderId, $amount, $cardToken, $description = '')
    {
        //preg_match("/Выдача средств по контракту (\d+) \(([^\(\)]+)\)/", $description, $matches);
        $phone = $this->phone;

        if (!$phone) {
            throw new \Exception('Не указан номер телефона');
        }

        $params = [
            'card' => $cardToken,
            'amount' => $amount,
            'extid' => Carbon::now()->timestamp . '_' .$phone . '_' . rand(1000, 9999),
            'document_id' => $this->contractName ?: $description,
            'fullname' => $this->fio ?: $description,
            'benificphone' => mb_substr( $phone, 1)
        ];

        return $this->transaction($params, 'out/paycard', 'POST');
    }

    /**
     * Оплата
     * @param $orderId
     * @param float $amount
     * @param string $description
     * @param float $fee
     * @return Response
     * @throws \Exception
     */
    public function pay($orderId, $amount, $description = '', $fee = 0)
    {
        $amount = $amount + $fee;
        $amount = ceil($amount * 100) / 100;

        $phone = $this->phone;
        if (!$phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        //preg_match("/Прием платежа по контракту (\d+) \(([^\(\)]+)\)/", $description, $matches);

        $params = [
            'amount' => $amount,
            'currency' => 'RUB',
            'document_id' => $this->contractName ?: $description,
            'fullname' => $this->fio ?: $description,
            'phone' => mb_substr( $phone, 1),
            'extid' => Carbon::now()->timestamp . '_' .$phone . '_' . rand(1000, 9999),
            'timeout' => 59,
            'successurl' => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl,
            'failurl' => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl,
            'cancelurl' => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl,
        ];

        return $this->transaction($params, $method = 'pay/lk', $request = 'POST');
    }

    /**
     * todo
     * Повторное списание средств на основе ID обычной оплаты
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return \ITPolice\PaymentSystems\Systems\MandarinBank\Response
     * @throws \Exception
     */
    public function recurring($orderId, $amount, $cardToken, $description = '') {
        // todo
    }

    /**
     * Безакцептное списание средств по токену
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function recurringByToken($orderId, $amount, $cardToken, $description = '')
    {
        $phone = $this->phone;

        if (!$phone) {
            throw new \Exception('Не указан номер телефона');
        }

        $params = [
            'card'   => $cardToken,
            'amount' => $amount,
            'extid'  => Carbon::now()->timestamp . '_' .$phone . '_' . rand(1000, 9999)
        ];

        return $this->transaction($params, 'pay/auto', 'POST');
    }

    public function setCallbackRequest(Request $request, $POST = null, $GET = null)
    {
        $this->callbackRequest = new CallbackRequest($request->all());
        $this->callbackRequest->setPost($POST);
        $this->callbackRequest->setGet($_GET);
        $this->callbackRequest->setMerchant($this->merchantId);
        $this->callbackRequest->setSecret($this->secret);
        return $this;
    }

    public function responseCallback(): string
    {
        return 'OK';
    }

    public function callbackAfterSetOperation(PaymentSystemTransaction $previousModel)
    {
        if($previousModel) {
            $this->setMerchant($previousModel->merchant);
        }
    }

    /**
     * Получение базового URL
     * @return string
     */
    protected function getBaseUrl()
    {
        if (env('PAY_SYSTEM_TEST_MODE')) {
            $baseUrl = 'https://test.impay.ru/v1/';
        } else {
            $baseUrl = 'https://api.impay.ru/v1/';
        }

        return $baseUrl;
    }

    /**
     * Безакцептное списание средств по токену
     * @param $cardID
     * @param $type
     * @return Response
     * @throws \Exception
     */
    public function getCard($cardID, $type = 0)
    {
        $params = [
            'id' => $cardID,
            'type' => $type
        ];

        return $this->transaction($params, 'card/get', 'POST')->toArray();
    }

    public function hasMultiPayments(): bool
    {
        /**
         * todo
         * Проверить возможность использования
         * и при необходимости дополнить параметры в методе pay()
         */
        return false;
    }

    public function getCardsPayIn(): array
    {
        throw new \Exception('Method not available');
    }

    public function getCardsPayOut(): array
    {
        throw new \Exception('Method not available');
    }

    public function reverseCardBinding(PaymentSystemTransaction $model)
    {
        // TODO: Implement reverseCardBinding() method.
    }

    public function orderInfo(string $orderId)
    {
        // TODO: Implement orderInfo() method.
    }
}
