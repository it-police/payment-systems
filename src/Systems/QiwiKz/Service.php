<?php

namespace ITPolice\PaymentSystems\Systems\QiwiKz;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use ITPolice\PaymentSystems\Interfaces\ServiceInterface;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;
use ITPolice\PaymentSystems\Traits\ServiceTrait;

class Service implements ServiceInterface
{
    use ServiceTrait;

    protected static $systemCode = 'qiwikz';

    /**
     * Метод привязки карты
     * @return Response
     * @throws \Exception
     */
    public function cardBinding()
    {
        $phone = $this->preparePhone($this->phone);
        if ( ! $phone) {
            throw new \Exception('Не указан номер телефона');
        }

        $email = (string)$this->email;
        if ( ! $email) {
            $email = $this->defaultEmail;
        }

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email');
        }

        $reference = Carbon::now()->timestamp.'_'.$phone.'_'.rand(1000, 9999);

        $params = [
            'billId'             => $reference,
            'amount'             => [
                'value' => env('PAY_SYSTEM_CARD_BINDING_AMOUNT', 1),
            ],
            'expirationDateTime' => Carbon::now('Asia/Almaty')->addHours(2)->toIso8601String(),
            'comment'            => 'Привязка карты',
            'flags'              => ['SALE', 'BIND_PAYMENT_TOKEN'],
            'successUrl'         => $this->returnUrl ?: $this->defaultReturnUrl,
            'customer'           => [
                'account' => $this->userId,
                'phone'   => $phone,
                'email'   => $email,
            ]
        ];

        $response = $this->register($params);

        if ( ! $response->success()) {
            return $response;
        }

        $response->setLink($response->link());

        return $response;
    }

    /**
     * Регистрация заказа
     *
     * @param $data
     *
     * @return Response
     * @throws \Exception
     */
    protected function register($data)
    {
        $data['amount']['currency'] = 'KZT';

        $billId = $data['billId'];
        unset($data['billId']);

        return $this->transaction(sprintf('/sites/%s/bills/%s', $this->merchantId, $billId), $data, 'PUT');
    }

    /**
     * @param $request
     * @param $params
     *
     * @return Response
     * @throws \Exception
     */
    protected function transaction($request, $params, string $method = 'POST')
    {
        $url        = $this->getBaseUrl().$request;
        $postParams = json_encode($params, JSON_UNESCAPED_UNICODE);
        $startTime  = microtime(true);

        if (isset($params['amount']['value'])) {
            $params['amount']['value'] = intval($params['amount']['value']);
        }

        $ch = curl_init();
        if (strtolower($method) !== 'get') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
        } else {
            $url .= '?'.http_build_query($params);
        }

        curl_setopt_array($ch, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 310,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSLVERSION     => 6,
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_HTTPHEADER     => array(
                "Authorization: Bearer $this->secret",
                "Content-Type: application/json",
                "Accept: application/json",
            ),
        ));
        $res      = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $responseObj = null;
        try {
            $responseObj = json_decode($res);

            $requestExecutionTime = microtime(true) - $startTime;

            Log::debug('QiwiKz response '.$request, [
                'params'   => $params,
                'httpCode' => $httpCode,
                'response' => (array)$responseObj,
                'execTime' => $requestExecutionTime
            ]);
        } catch (\Throwable $e) {
            Log::error('QiwiKz transaction error', [
                'exception' => $e,
                'httpCode'  => $httpCode,
                'response'  => $res,
            ]);
        }

        return new Response($responseObj);
    }

    /**
     * Получение базового URL
     * @return string
     */
    protected function getBaseUrl()
    {
        if (env('PAY_SYSTEM_TEST_MODE')) {
            $baseUrl = 'https://api-test.qiwip.kz';
        } else {
            $baseUrl = 'https://api.qiwip.kz';
        }

        return $baseUrl;
    }

    /**
     * Выдача средств
     *
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     *
     * @return Response
     * @throws \Exception
     */
    public function payoutByToken($orderId, $amount, $cardToken, $description = 'Выдача')
    {
        throw new \Exception('Method not available');
    }

    /**
     * Оплата
     *
     * @param $orderId
     * @param float $amount
     * @param string $description
     * @param float $fee
     *
     * @return Response
     * @throws \Exception
     */
    public function pay($orderId, $amount, $description = '', $fee = 0)
    {
        $phone = $this->preparePhone($this->phone);
        if ( ! $phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if ( ! $email) {
            $email = $this->defaultEmail;
        }

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $amount    = ceil(($amount + $fee) * 100) / 100;
        $reference = Carbon::now()->timestamp.'_'.$phone.'_'.rand(1000, 9999);

        $params = [
            'billId'             => $reference,
            'amount'             => [
                'value' => $amount
            ],
            'expirationDateTime' => Carbon::now('Asia/Almaty')->addHours(2)->toIso8601String(),
            'comment'            => $description,
            'flags'              => ['SALE'],
            'successUrl'         => $this->returnUrl ?: $this->defaultReturnUrl,
            'customer'           => [
                'account' => $this->userId,
                'phone'   => $phone,
                'email'   => $email,
            ]
        ];

        $response = $this->register($params);

        if ( ! $response->success()) {
            return $response;
        }

        $response->setLink($response->link());

        return $response;
    }

    /**
     * todo
     * Повторное списание средств на основе ID обычной оплаты
     *
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     *
     * @return \ITPolice\PaymentSystems\Systems\MandarinBank\Response
     * @throws \Exception
     */
    public function recurring($orderId, $amount, $cardToken, $description = '')
    {
        throw new \Exception('Method not available');
    }

    /**
     * Безакцептное списание средств по токену
     *
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     *
     * @return Response
     * @throws \Exception
     */
    public function recurringByToken($orderId, $amount, $cardToken, $description = 'Регулярный платеж')
    {
        $amount = intval($amount * 100);

        $phone = $this->preparePhone($this->phone);
        if ( ! $phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if ( ! $email) {
            $email = $this->defaultEmail;
        }

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $params = [
            'paymentMethod' => [
                'type'         => 'TOKEN',
                'paymentToken' => $cardToken,
            ],
            'amount'        => [
                'value'    => $amount,
                'currency' => 'KZT',
            ],
            'comment'       => $description,
            'flags'         => ['SALE'],
            'customer'      => [
                'account' => $this->userId,
                'phone'   => $phone,
                'email'   => $email,
            ]
        ];

        $reference = Carbon::now()->timestamp.'_'.$phone.'_'.rand(1000, 9999);

        return $this->transaction(sprintf('/sites/%s/payments/%s', $this->merchantId, $reference), $params, 'PUT');
    }

    public function setCallbackRequest(Request $request, $POST = null, $GET = null)
    {
        $signature = $request->header('Signature');
        $request->request->add(['signature' => $signature]);
        $this->callbackRequest = new CallbackRequest($request->all());
        $this->callbackRequest->setMerchant($this->merchantId);
        $this->callbackRequest->setSecret($this->secret);

        return $this;
    }

    public function responseCallback(): string
    {
        return 'ok';
    }

    public function callbackAfterSetOperation(PaymentSystemTransaction $previousModel)
    {
    }

    public function hasMultiPayments(): bool
    {
        return false;
    }

    public function getCardsPayIn(): array
    {
        throw new \Exception('Method not available');
    }

    public function getCardsPayOut(): array
    {
        throw new \Exception('Method not available');
    }

    public function reverseCardBinding(PaymentSystemTransaction $model)
    {
        // TODO: Implement reverseCardBinding() method.
    }

    public function orderInfo(string $orderId)
    {
        // TODO: Implement orderInfo() method.
    }
}
