<?php

namespace ITPolice\PaymentSystems\Systems\QiwiKz;

use Illuminate\Support\Facades\Log;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;

class CallbackRequest implements CallbackRequestInterface
{
    use CallbackRequestTrait;

    /**
     * Метод проверки подписи
     *
     * @param array $request
     *
     * @return bool
     */
    public function checkSign()
    {
        $request = $this->request;
        $sign    = $request['signature'];
        unset($request['signature']);

        $signature = null;
        switch ($request['type']) {
            case 'PAYMENT':
                $signature = implode('|', [
                    $request['payment']['paymentId'],
                    $request['payment']['createdDateTime'] ?? $request['payment']['createDateTime'],
                    number_format($request['payment']['amount']['value'], 2),
                ]);
                break;
            case 'CAPTURE':
                $signature = implode('|', [
                    $request['capture']['captureId'],
                    $request['capture']['createdDateTime'] ?? $request['capture']['createDateTime'],
                    number_format($request['capture']['amount']['value'], 2),
                ]);
                break;
            case 'REFUND':
                $signature = implode('|', [
                    $request['refund']['refundId'],
                    $request['refund']['createdDateTime'] ?? $request['refund']['createDateTime'],
                    number_format($request['refund']['amount']['value'], 2)
                ]);
                break;
        }
        $signature = hash_hmac('sha256', $signature, $this->secret);

        if ($signature !== $sign) {
            Log::debug('QiwiKz signature error:', [
                'signature' => $signature,
                'sign'      => $sign,
            ]);
        }

        return $signature === $sign;
    }

    public function transactionId()
    {
        $transactionId = @$this->request['payment']['paymentId'];
        $object_type   = @$this->request['type'];

        switch ($object_type) {
            case 'PAYMENT':
                if ($this->request['payment']['paymentMethod']['type'] !== 'TOKEN') {
                    return @$this->request['payment']['billId'];
                }
                break;
            case 'CAPTURE':
                return @$this->request['capture']['billId'];
            case 'REFUND':
                return @$this->request['refund']['billId'];
        }

        return $transactionId;
    }

    public function action()
    {
        $object_type = @$this->request['type'];
        if ($object_type == 'PAYMENT') {
            if ( ! empty($this->request['payment']['tokenData']['paymentToken']) && @$this->request['payment']['paymentMethod']['type'] === 'CARD') {
                return self::ACTION_CARD_BINDING;
            }

            return self::ACTION_PAY;
        }

        return null;
    }

    public function errorMessage(): string
    {
        $object_type = @$this->request['type'];
        $reason      = '';

        return match ($object_type) {
            'PAYMENT' => $this->request['payment']['status']['reasonMessage'] ?? $reason,
            'CAPTURE' => $this->request['capture']['status']['reasonMessage'] ?? $reason,
            'REFUND' => $this->request['refund']['status']['reasonMessage'] ?? $reason,
            default => $reason,
        };
    }

    public function amount(): float
    {
        return (float)@$this->request['amount'] / 100;
    }

    public function bankCard(): BankCard
    {
        $bankCard = new BankCard();

        $token   = $this->request['payment']['tokenData']['paymentToken'];
        $cardBin = mb_substr($this->request['payment']['paymentMethod']['maskedPan'], 0, 6);
        $cardPan = mb_substr($this->request['payment']['paymentMethod']['maskedPan'], -4);

        $bankCard->token    = $token;
        $bankCard->bin      = $cardBin;
        $bankCard->pan      = $cardPan;
        $bankCard->month    = null;
        $bankCard->year     = null;
        $bankCard->holder   = 'UNKNOWN NAME';
        $bankCard->bankName = null;

        return $bankCard;
    }

    public function isStopError(): bool
    {
        return match ($this->errorCode()) {
            'DENY_PAY_BY_TOKEN', 'ACQUIRING_PROXY_3DS_ERROR', 'ACQUIRING_VALIDATION_ERROR', 'ACQUIRING_LIMIT_EXCEEDED', 'ACQUIRING_SUSPECTED_FRAUD', 'ACQUIRING_EXPIRED_CARD', 'ACQUIRING_INVALID_CARD', 'VALIDATION_INVALID_SITE_SIGN_HMAC', 'PAYMENT_NOT_FOUND', 'INVOICE_ALREADY_PAID', 'INVALID_STATE' => true,
            default => false,
        };
    }

    public function status()
    {
        $object_type = @$this->request['type'];
        $status      = 'error';
        switch ($object_type) {
            case 'CAPTURE':
                $status = @$this->request['capture']['status']['value'];
                break;
            case 'REFUND':
                $status = @$this->request['refund']['status']['value'];
                break;
            case 'PAYMENT':
                $status = @$this->request['payment']['status']['value'];
                break;
        }

        return match ($status) {
            'SUCCESS', 'COMPLETED' => self::STATUS_SUCCESS,
            'DECLINE' => self::STATUS_ERROR,
            default => $status,
        };
    }

    public function errorCode()
    {
        $object_type = @$this->request['type'];
        $errorCode   = '';

        return match ($object_type) {
            'PAYMENT' => $this->request['payment']['status']['reasonCode'] ?? $errorCode,
            'CAPTURE' => $this->request['capture']['status']['reasonCode'] ?? $errorCode,
            'REFUND' => $this->request['refund']['status']['reasonCode'] ?? $errorCode,
            default => @$errorCode,
        };
    }

    public function isFailedError(): bool
    {
        // todo
        return false;
    }
}
