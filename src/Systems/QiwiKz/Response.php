<?php

namespace ITPolice\PaymentSystems\Systems\QiwiKz;

use ITPolice\PaymentSystems\Interfaces\ResponseInterface;
use ITPolice\PaymentSystems\Traits\ResponseTrait;

class Response implements ResponseInterface
{
    use ResponseTrait;

    public function parse($response)
    {
        if (isset($response->errorCode)) {
            $this->errorMessage = (string)(@$response->message ?? @$response->errorCode);
        } else {
            /** Так как id платежа нам не отдают при формировании ссылки на оплату - работаем с id счета */
            $this->id = $response->paymentId ?? @$response->billId;

            if ( ! empty($response->status->value) && $response->status->value !== 'DECLINE') {
                $this->success = true;
            }

            /** Ошибка может быть и в status.reason */
            if ( ! empty($response->status->reason)) {
                $this->errorMessage = $response->status->reason;
            }

            $this->link = $response->payUrl ?? null;
        }
    }
}
