<?php

namespace ITPolice\PaymentSystems\Systems\SpareBest2Pay;


use ITPolice\PaymentSystems\Interfaces\ResponseInterface;
use ITPolice\PaymentSystems\Traits\ResponseTrait;

class Response implements ResponseInterface
{
    use ResponseTrait;

    public function parse($response)
    {
        if(isset($response->order_id)) {
            $this->id = @$response->order_id;
        } else {
            $this->id = @$response->id;
        }
        if (isset($response->state) && $response->state !== 'REJECTED' && $response->state !== 'ERROR') {
            $this->success = true;
        } else {
            $this->errorMessage = (string) (@$response->description ?? @$response->message);
        }
    }
}
