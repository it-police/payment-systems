<?php

namespace ITPolice\PaymentSystems\Systems\Payselection;

use ITPolice\PaymentSystems\Interfaces\ResponseInterface;
use ITPolice\PaymentSystems\Traits\ResponseTrait;

class Response implements ResponseInterface
{
    use ResponseTrait;

    public function __construct($response, ?int $httpCode = null)
    {
        if ($httpCode >= 400) {
            $level = (int)\floor($httpCode / 100);
            if ($level === 4) {
                $message = 'Client error.';
            } elseif ($level === 5) {
                $message = 'Server error.';
            } else {
                $message = 'Unsuccessful request.';
            }

            if ( ! json_last_error()) {
                if ( ! empty($response->Code)) {
                    $message .= sprintf(' Code error: %s.', $response->Code);
                }

                if ( ! empty($response->Description)) {
                    $message .= sprintf(' Description error: %s.', $response->Description);
                }
            }

            $this->errorMessage = $message;
        }

        $this->response = $response;
        $this->isNull   = ! is_object($response);
        $this->parse($response);
    }

    public function parse($response)
    {
        $this->success = false;

        if (empty($this->errorMessage)) {
            $this->id = @$response->Invoice ?? @$response->OrderId;

            $this->success = true;

            $this->link = $response->Url ?? null;
        }
    }
}
