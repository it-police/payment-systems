<?php

namespace ITPolice\PaymentSystems\Systems\Payselection;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use ITPolice\PaymentSystems\Interfaces\ServiceInterface;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;
use ITPolice\PaymentSystems\Systems\Payselection\Enum\PaymentMethod;
use ITPolice\PaymentSystems\Systems\Payselection\Enum\PaymentType;
use ITPolice\PaymentSystems\Systems\Payselection\Enum\PSMethodsEnum;
use ITPolice\PaymentSystems\Traits\ServiceTrait;

class Service implements ServiceInterface
{
    use ServiceTrait;

    protected static $systemCode = 'payselection';

    /**
     * Метод привязки карты
     * @return Response
     * @throws \Exception
     */
    public function cardBinding()
    {
        $phone = $this->preparePhone($this->phone);
        if ( ! $phone) {
            throw new \Exception('Не указан номер телефона');
        }

        $email = (string)$this->email;
        if ( ! $email) {
            $email = $this->defaultEmail;
        }

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email');
        }

        $reference = Carbon::now()->timestamp.'_'.$phone.'_'.rand(1000, 9999);

        $params = [
            'MetaData'       => [
                'PaymentType' => PaymentType::BLOCK,
                'PreviewForm' => false,
                'SendSMS'     => false,
                'SendBill'    => false
            ],
            'PaymentRequest' => [
                'OrderId'     => $reference,
                'Amount'      => $this->roundNumber(env('PAY_SYSTEM_CARD_BINDING_AMOUNT', 1)),
                'Currency'    => 'RUB',
                'Description' => 'Привязка карты',
                'RebillFlag'  => true,
                "ExtraData"   => [
                    'ReturnUrl'        => $this->returnUrl ?: $this->defaultReturnUrl,
                    'WebhookUrl'       => $this->callbackUrl,
                    'ShortDescription' => 'Привязка карты',
                ]
            ],
            'CustomerInfo'   => [
                'Phone' => '+7'.$phone,
                'Email' => $email,
            ],
        ];

        return $this->register($params);
    }

    /**
     * Регистрация заказа
     *
     * @param $data
     *
     * @return Response
     * @throws \Exception
     */
    protected function register($data)
    {
        return $this->transaction(PSMethodsEnum::PAYMENTS_WEBPAY_LINK, $data);
    }

    /**
     * @param $request
     * @param $params
     *
     * @return Response
     * @throws \Exception
     */
    protected function transaction($request, $params, string $method = 'POST')
    {
        $baseUrl = $this->getBaseUrl();

        if (in_array($request, [PSMethodsEnum::PAYMENTS_WEBPAY, PSMethodsEnum::PAYMENTS_WEBPAY_LINK])) {
            $baseUrl = 'https://webform.payselection.com';
        }

        $uid = \Str::uuid()->toString();
        $msg = $method.PHP_EOL.$request.PHP_EOL.$this->merchantId.PHP_EOL.$uid.PHP_EOL.json_encode($params,
                JSON_UNESCAPED_UNICODE);

        $headers = [
            'X-SITE-ID: '.$this->merchantId,
            'X-REQUEST-ID: '.$uid,
            'X-REQUEST-SIGNATURE: '.$this->getSignature($msg, $this->secret),
            'Content-Type: application/json',
            'Accept: application/json',
        ];

        $url        = $baseUrl.$request;
        $postParams = json_encode($params, JSON_UNESCAPED_UNICODE);
        $startTime  = microtime(true);

        $ch = curl_init();
        if (strtolower($method) !== 'get') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
        } else {
            $url .= '?'.http_build_query($params);
        }

        curl_setopt_array($ch, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 310,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSLVERSION     => 6,
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_HTTPHEADER     => $headers,
        ));
        $res      = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $responseObj = null;
        try {
            $responseObj = json_decode($res);
            if ($httpCode >= 400) {
                throw new \Exception($responseObj->Code ?? $responseObj->Description ?? $res, $httpCode);
            }
            $requestExecutionTime = microtime(true) - $startTime;

            Log::debug('Payselection response '.$request, [
                'params'   => $params,
                'httpCode' => $httpCode,
                'response' => (array)$responseObj,
                'execTime' => $requestExecutionTime
            ]);
        } catch (\Throwable $e) {
            Log::error('Payselection transaction error', [
                'exception' => $e,
                'httpCode'  => $httpCode,
                'response'  => $res,
            ]);
        }

        return new Response($responseObj, $httpCode);
    }

    /**
     * Получение базового URL
     * @return string
     */
    protected function getBaseUrl()
    {
        return 'https://gw.payselection.com';
    }

    /**
     * Выдача средств
     *
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     *
     * @return Response
     * @throws \Exception
     */
    public function payoutByToken($orderId, $amount, $cardToken, $description = 'Выдача')
    {
        $phone = $this->preparePhone($this->phone);
        if ( ! $phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if ( ! $email) {
            $email = $this->defaultEmail;
        }

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $params = [
            'OrderId'       => $orderId,
            'Amount'        => $this->roundNumber($amount),
            'Currency'      => 'RUB',
            'Description'   => $description,
            "ExtraData"     => [
                'WebhookUrl' => $this->callbackUrl,
            ],
            'CustomerInfo'  => [
                'Phone' => '+7'.$phone,
                'Email' => $email,
            ],
            'PayoutMethod'  => PaymentMethod::TOKEN,
            'PayoutDetails' => [
                'PayoutToken' => $cardToken,
            ]
        ];

        return $this->transaction(PSMethodsEnum::PAYMENTS_PAYOUT, $params);
    }

    /**
     * Оплата
     *
     * @param $orderId
     * @param float $amount
     * @param string $description
     * @param float $fee
     *
     * @return Response
     * @throws \Exception
     */
    public function pay($orderId, $amount, $description = '', $fee = 0)
    {
        $phone = $this->preparePhone($this->phone);
        if ( ! $phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if ( ! $email) {
            $email = $this->defaultEmail;
        }

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $amount    = ceil(($amount + $fee) * 100) / 100;
        $reference = Carbon::now()->timestamp.'_'.$phone.'_'.rand(1000, 9999);

        $params = [
            'MetaData'       => [
                'PaymentType' => PaymentType::PAY,
                'PreviewForm' => false,
                'SendSMS'     => false,
                'SendBill'    => false
            ],
            'PaymentRequest' => [
                'OrderId'     => $reference,
                'Amount'      => $this->roundNumber($amount),
                'Currency'    => 'RUB',
                'Description' => $description,
                "ExtraData"   => [
                    'ReturnUrl'        => $this->returnUrl ?: $this->defaultReturnUrl,
                    'WebhookUrl'       => $this->callbackUrl,
                    'ShortDescription' => $description,
                ]
            ],
            'CustomerInfo'   => [
                'Phone' => '+7'.$phone,
                'Email' => $email,
            ],
        ];

        $response = $this->register($params);

        return $response;
    }

    /**
     * todo
     * Повторное списание средств на основе ID обычной оплаты
     *
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     *
     * @return \ITPolice\PaymentSystems\Systems\MandarinBank\Response
     * @throws \Exception
     */
    public function recurring($orderId, $amount, $cardToken, $description = '')
    {
        throw new \Exception('Method not available');
    }

    /**
     * Безакцептное списание средств по токену
     *
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     *
     * @return Response
     * @throws \Exception
     */
    public function recurringByToken($orderId, $amount, $cardToken, $description = 'Регулярный платеж')
    {
        $phone = $this->preparePhone($this->phone);
        if ( ! $phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if ( ! $email) {
            $email = $this->defaultEmail;
        }

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $params = [
            'OrderId'     => $orderId,
            'Amount'      => $this->roundNumber($amount),
            'Currency'    => 'RUB',
            'Description' => $description,
            'RebillId'    => $cardToken,
            'PaymentType' => PaymentType::PAY,
            'WebhookUrl'  => $this->callbackUrl,
        ];

        return $this->transaction(PSMethodsEnum::PAYMENTS_REBILL, $params);
    }

    public function setCallbackRequest(Request $request, $POST = null, $GET = null)
    {
        $signature = $request->header('X-WEBHOOK-SIGNATURE');
        $request->request->add(['signature' => $signature]);
        $siteId = $request->header('X-SITE-ID');
        $request->request->add(['siteId' => $siteId]);
        $request->request->add(['raw' => $request->getContent()]);
        $this->callbackRequest = new CallbackRequest($request->all());
        $this->callbackRequest->setMerchant($this->merchantId);
        $this->callbackRequest->setSecret($this->secret);

        return $this;
    }

    public function responseCallback(): string
    {
        return 'ok';
    }

    public function callbackAfterSetOperation(PaymentSystemTransaction $previousModel)
    {
    }

    public function hasMultiPayments(): bool
    {
        return false;
    }

    public function getCardsPayIn(): array
    {
        throw new \Exception('Method not available');
    }

    public function getCardsPayOut(): array
    {
        throw new \Exception('Method not available');
    }

    public function reverseCardBinding(PaymentSystemTransaction $model)
    {
        try {
            $amount   = intval(env('PAY_SYSTEM_CARD_BINDING_AMOUNT', 1));

            /** Дополнительно проверяем, что в транзакции была привязка карты */
            if ($model->operation == 'cardBinding') {
                $params = [
                    'TransactionId' => $model->card_token,
                    'Amount' => $this->roundNumber($amount),
                    'Currency' => 'RUB',
                    'WebhookUrl' => $this->callbackUrl,
                ];

                $this->transaction(PSMethodsEnum::PAYMENTS_CANCEL, $params);
            }
        } catch (\Throwable $e) {
            Log::error('Payselection reverse card binding error', [
                'exception' => $e
            ]);
        }
    }

    public function orderInfo(string $orderId)
    {
        // TODO: Implement orderInfo() method.
    }


    /**
     * @param string $body
     * @param string $secretKey
     *
     * @return string
     */
    private function getSignature(string $body, string $secretKey): string
    {
        return hash_hmac('sha256', $body, $secretKey);
    }

    private function roundNumber($num): string
    {
        return number_format($num, 2, '.', '');
    }
}
