<?php

namespace ITPolice\PaymentSystems\Systems\Payselection\Enum;

class PaymentType
{
    public const PAY   = 'Pay';
    public const BLOCK = 'Block';
}
