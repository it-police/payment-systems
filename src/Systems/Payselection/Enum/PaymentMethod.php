<?php

namespace ITPolice\PaymentSystems\Systems\Payselection\Enum;

class PaymentMethod
{
    public const CARD = 'Card';
    public const TOKEN = 'Token';
}
