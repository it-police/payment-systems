<?php

namespace ITPolice\PaymentSystems\Systems\Payselection\Enum;

class PSMethodsEnum
{
    /** Создание Webpay платежа */
    public const PAYMENTS_WEBPAY = '/webpayments/create';

    /** Создание ссылки на оплату */
    public const PAYMENTS_WEBPAY_LINK = '/webpayments/paylink_create';

    /** Отмена холда */
    public const PAYMENTS_CANCEL = '/payments/cancellation';

    /** Безакцептное списание */
    public const PAYMENTS_REBILL = '/payments/requests/rebill';

    /** Вывод средств */
    public const PAYMENTS_PAYOUT = '/payouts';
}
