<?php

namespace ITPolice\PaymentSystems\Systems\Payselection;

use Illuminate\Support\Facades\Log;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;

class CallbackRequest implements CallbackRequestInterface
{
    use CallbackRequestTrait;

    /**
     * Метод проверки подписи
     *
     * @param array $request
     *
     * @return bool
     */
    public function checkSign()
    {
        $request = $this->request;
        $sign    = $request['signature'];
        $siteId  = $request['siteId'];
        unset($request['signature'], $request['siteId']);

        $callbackUrl = route('pay-system.callback', ['system' => Service::getSystemCode()]);

        $msg       = \request()->getMethod().PHP_EOL.$callbackUrl.PHP_EOL.$this->merchantId.PHP_EOL.$request['raw'];
        $signature = hash_hmac('sha256', $msg, $this->secret);

        if ($signature !== $sign) {
            Log::debug('Payselection signature error:', [
                'request'   => $request['raw'],
                'signature' => $signature,
                'sign'      => $sign,
            ]);
        }

        return $signature === $sign;
    }

    public function transactionId()
    {
        return @$this->request['OrderId'];
    }

    public function action()
    {
        $object_type = @$this->request['Event'];
        $description = @$this->request['Description'];
        if ($object_type == 'Block' || $description == 'Привязка карты') {
            return self::ACTION_CARD_BINDING;
        }
        if ($object_type == 'Payment' || $object_type == 'Fail') {
            return self::ACTION_PAY;
        }
        if ($object_type == 'Payout') {
            return self::ACTION_PAYOUT;
        }

        return null;
    }

    public function errorMessage(): string
    {
        return $this->request['ErrorMessage'] ?? '';
    }

    public function amount(): float
    {
        return (float)@$this->request['Amount'];
    }

    public function bankCard(): BankCard
    {
        $bankCard = new BankCard();

        $token   = $this->request['RebillId'];
        $cardBin = mb_substr($this->request['CardMasked'], 0, 6);
        $cardPan = mb_substr($this->request['CardMasked'], -4);

        $bankCard->token        = $token;
        $bankCard->bin          = $cardBin;
        $bankCard->pan          = $cardPan;
        $bankCard->month        = null;
        $bankCard->year         = null;
        $bankCard->holder       = $this->request['CardHolder'] ?? 'UNKNOWN NAME';
        $bankCard->bankName     = $this->request['Bank'] ?? null;
        $bankCard->payout_token = $this->request['PayoutToken'] ?? null;

        return $bankCard;
    }

    public function isStopError(): bool
    {
        return match ($this->errorCode()) {
            '7002', '7005', '7006', '7010', '7011', '7012', '7020', '7021', '7024', '7025' => true,
            default => false,
        };
    }

    public function status()
    {
        $object_type = @$this->request['Event'];
        $status      = 'error';
        switch ($object_type) {
            case 'Block':
                if ( ! empty($this->request['RebillId']) && $this->request['PaymentMethod'] === 'Card') {
                    $status = self::STATUS_SUCCESS;
                }
                break;
            case 'Payment':
            case 'Payout':
            case 'Cancel':
                $status = self::STATUS_SUCCESS;
                break;
            case '3DS':
            case 'Redirect3DS':
                $status = self::STATUS_NEW;
                break;
            case 'Fail':
                $status = self::STATUS_ERROR;
                break;
        }

        return $status;
    }

    public function errorCode()
    {
        return @$this->request['ErrorCode'];
    }

    public function isFailedError(): bool
    {
        return false;
    }
}
