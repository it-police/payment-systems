<?php

namespace ITPolice\PaymentSystems\Systems\GreenLeavesPay;

use Illuminate\Support\Str;
use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Systems\GreenLeavesPay\Service;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;

class CallbackRequest implements CallbackRequestInterface
{
    use CallbackRequestTrait;

    /**
     * Метод проверки подписи
     * @param array $request
     * @return bool
     */
    public function checkSign()
    {
        return true;
    }

    public function transactionId()
    {
        return $this->request['pg_payment_id'];
    }

    public function action()
    {
        $object_type = @$this->request['pg_type'];
        if ($object_type == 'pay') {
            return self::ACTION_PAY;
        } elseif ($object_type == 'add') {
            return self::ACTION_CARD_BINDING;
        }
        return null;
    }

    public function errorMessage(): string
    {
        return Str::limit(
            $this->request['pg_failure_description'] 
            ?? $this->request['pg_error_description'] 
            ?? @$this->request['pg_error_status']
        , 220);
    }

    public function amount(): float
    {
        return (float)@$this->request['amount'] / 100;
    }

    public function bankCard(): BankCard
    {
        // todo parse card
        $bankCard = new BankCard();

        $token = @$this->request['pg_card_token'];
        $cardBin = mb_substr(@$this->request['pg_card_hash'], 0, 6);
        $cardPan = mb_substr(@$this->request['pg_card_hash'], -4);
        $cardMonth = @$this->request['pg_card_month'];
        $cardYear = @$this->request['pg_card_year'];
        $cardHolder = '';

        $bankCard->token = $token;
        $bankCard->bin = $cardBin;
        $bankCard->pan = $cardPan;
        $bankCard->month = $cardMonth;
        $bankCard->year = $cardYear;
        $bankCard->holder = $cardHolder;

        return $bankCard;
    }

    public function isStopError(): bool
    {
        switch ((int)$this->errorCode()) {
            case 2:
            case 3:
                return true;
        }
        return false;
    }

    public function status()
    {
        /** 
         * Если есть pg_result - проверяем по нему состояние платежа
         * 2 – Не завершен, 1 – успех, 0 – неудача. 
         */
        if(isset($this->request['pg_result'])) {
            $result = (int)$this->request['pg_result'];

            return match ($result) {
                0 => self::STATUS_ERROR,
                1 => self::STATUS_SUCCESS,
                default => self::STATUS_NEW,
            };
        }
        
        /** Иначе проверяем по pg_status */
        $status = @$this->request['pg_status'];
        return match ($status) {
            'error' => self::STATUS_ERROR,
            'ok' => self::STATUS_SUCCESS,
            default => self::STATUS_NEW,
        };
    }

    public function errorCode()
    {
        return $this->request['pg_failure_code'] ?? $this->request['pg_error_status'] ?? '';
    }

    public function isFailedError(): bool
    {
        // todo
        return false;
    }
}
