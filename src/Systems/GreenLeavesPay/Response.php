<?php

namespace ITPolice\PaymentSystems\Systems\GreenLeavesPay;


use ITPolice\PaymentSystems\Interfaces\ResponseInterface;
use ITPolice\PaymentSystems\Traits\ResponseTrait;

class Response implements ResponseInterface
{
    use ResponseTrait;

    public function parse($response)
    {
        $this->id = @$response->pg_payment_id??@$response->pg_salt;
        $this->success = @$response->pg_status!='error';
        $this->link = $response->pg_redirect_url??"";
        $this->errorMessage = (string)@$response->pg_error_description??"";

    }
}
