<?php

namespace ITPolice\PaymentSystems\Systems\GreenLeavesPay;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use ITPolice\PaymentSystems\Interfaces\ServiceInterface;
use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;
use ITPolice\PaymentSystems\Traits\ServiceTrait;
use Illuminate\Http\Request;

class Service implements ServiceInterface
{
    use ServiceTrait;

    protected static $systemCode = 'greanleaves';
    public static $refIdPrefix = 'ref-';

    /**
     * Получение базового URL
     * @return string
     */
    protected function getBaseUrl()
    {
        if (env('PAY_SYSTEM_TEST_MODE')) {
            $baseUrl = 'https://api.greenleavespay.kz';
        } else {
            $baseUrl = 'https://api.greenleavespay.kz';
        }

        return $baseUrl;
    }

    /**
     * Метод привязки карты
     * @return Response
     * @throws \Exception
     */
    public function cardBinding()
    {
        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона');
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email');
        }

        $params = [
            'pg_merchant_id' => $this->merchantId,
            'pg_user_id'     => $this->userId,
            'pg_back_link'   => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl,
            'pg_post_link'   => $this->callbackUrl,
            'pg_salt'        => $this->generateRandomString(),
        ];

        $params['pg_sig'] = $this->signature($params,'add');

        return $this->transaction('/v1/merchant/' . $this->merchantId . '/cardstorage/add', $params);
    }



    /**
     * Функция превращает многомерный массив в плоский
     */
    protected function makeFlatParamsArray($arrParams, $parent_name = ''): array
    {
        $arrFlatParams = [];
        $i = 0;
        foreach ($arrParams as $key => $val) {
            $i++;
            $name = $parent_name . $key . sprintf('%03d', $i);
            if (is_array($val)) {
                $arrFlatParams = array_merge($arrFlatParams, $this->makeFlatParamsArray($val, $name));
                continue;
            }
            $arrFlatParams += array($name => (string)$val);
        }

        return $arrFlatParams;
    }

    /**
     * Функция генерации подписи
     */
    protected function signature($requestForSignature, $action)
    {
        $request = $this->makeFlatParamsArray($requestForSignature);
        ksort($request);
        array_unshift($request, $action);
        array_push($request, $this->secret);

        return md5(implode(';', $request));
    }

    /**
     * Функция генерации соли
     */
    protected function generateRandomString($length = 10) {

        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    /**
     * @param $method
     * @param $params
     * @return Response
     * @throws \Exception
     */
    protected function transaction($method, $params, $isPost = true)
    {
        $baseUrl = $this->getBaseUrl();

        $startTime = microtime(true);

        $url = $baseUrl . $method;

        $ch = curl_init();
        if ($isPost) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        curl_setopt_array($ch, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1

        ));
        $res = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $responseObj = null;
        try {
            $responseObj = json_decode(json_encode(simplexml_load_string($res), JSON_UNESCAPED_UNICODE));

            $requestExecutionTime = microtime(true) - $startTime;

            Log::debug('greanleaves response '.$method, [
                'params'   => $params,
                'httpCode' => $httpCode,
                'response' => $responseObj,
                'execTime' => $requestExecutionTime
            ]);
        } catch (\Throwable $e) {
            Log::error('greanleaves transaction error', [
                'exception' => $e,
                'httpCode'  => $httpCode,
                'response'  => $res,
            ]);
        }

        return new Response($responseObj);
    }



    /**
     * Выдача средств
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function payoutByToken($orderId, $amount, $cardToken, $description = 'Выдача')
    {
//        TODO

    }




    /**
     * Оплата
     * @param $orderId
     * @param float $amount
     * @param string $description
     * @param float $fee
     * @return Response
     * @throws \Exception
     */
    public function pay($orderId, $amount, $description = '', $fee = 0)
    {
        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $amount = $amount + $fee;

        $params = [
            'pg_merchant_id'          => $this->merchantId,
            'pg_user_id'              => $this->userId,
            'pg_order_id'             => $orderId,
            'pg_site_url'             => $this->returnUrl ?: $this->defaultReturnUrl,
            'pg_description'          => $description,
            'pg_amount'               => $amount,
            'pg_currency'             => 'KZT',
            'pg_result_url'           => $this->callbackUrl,
            'user_id'                 => $this->userId,
            'pg_user_phone'           => $phone,
            'pg_user_contact_email'   => $email,
            'pg_salt'                 => $this->generateRandomString(),
            'pg_success_url'          => $this->returnUrl ?: $this->defaultReturnUrl,
        ];
         $params['pg_sig'] = $this->signature($params,'init_payment.php');
        return $this->transaction('/init_payment.php', $params);
    }

    /**
     * todo
     * Повторное списание средств на основе ID обычной оплаты
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return \ITPolice\PaymentSystems\Systems\MandarinBank\Response
     * @throws \Exception
     */
    public function recurring($orderId, $amount, $cardToken, $description = '')
    {
        // todo
    }


    /**
     * Регистрация заказа
     * @param $data
     * @return Response
     * @throws \Exception
     */
    protected function register($data)
    {
        $data['pg_sig'] = $this->signature($data,'init');
        return $this->transaction('/v1/merchant/'.$this->merchantId.'/card/init', $data);
    }

    /**
     * Безакцептное списание средств по токену
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function recurringByToken($orderId, $amount, $cardToken, $description = 'Регулярный платеж')
    {
        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $referenceId = self::$refIdPrefix . $orderId;
        $paramsInit = [

            'pg_merchant_id'        => (int) $this->merchantId,
            'pg_amount'             => $amount,
            'pg_order_id'           => $orderId,
            'pg_user_id'            => $this->userId,
            'pg_card_token'         => $cardToken,
            'pg_description'        => $description,
            'pg_user_phone'         => $phone,
            'pg_user_contact_email' => $email,
            'pg_salt'               => $this->generateRandomString(),
        ];

        $register = self::register($paramsInit);

        if($register -> pg_status == 'ok'){
            $this->setId($referenceId);

            $pg_payment_id = $register->pg_payment_id;
            $params = [
                'pg_merchant_id' => (int) $this->merchantId,
                'pg_payment_id'  => $pg_payment_id,
                'pg_salt'        => $this->generateRandomString(),
            ];
            $params['pg_sig'] = $this->signature($paramsInit,'direct');

            $response = $this->transaction('/v1/merchant/'.$this->merchantId.'card/direct', $params);

        }else{
            $response = $register;
        }

        return $response;
    }

    public function getCardsPayIn(): array
    {
        $params = [
            'pg_merchant_id' => (int) $this->merchantId,
            'pg_post_link'   => $this->callbackUrl,
            'pg_user_id'     => $this->userId,
            'pg_salt'        => $this->generateRandomString(),
        ];

        $params['pg_sig'] = $this->signature($params,'list');
        $response = $this->transaction('/v1/merchant/'.$this->merchantId.'/cardstorage/list', $params);
        return $this->getCardModelsFromResponse($response);
    }


    //???
    public function getCardsPayOut(): array
    {
        return [];
    }

    /**
     * @param $response
     * @return BankCard[]
     */
    protected function getCardModelsFromResponse($response)
    {
        $cards = [];
        $card = (array)$response->getResponse()->card;
        $bankCard = new BankCard();
        $mask = preg_replace("/[^\d]+/", '', $card["pg_card_hash"]);
        $bankCard->token = $card["pg_card_token"];
        $bankCard->bin = mb_substr($mask, 0, 6);
        $bankCard->pan = mb_substr($mask, -4);
        $cards[] = $bankCard;

        return $cards;
    }

    public function setCallbackRequest(Request $request, $POST = null, $GET = null)
    {
        $data            = $POST;
        $data['pg_type'] = 'pay';

        if ( ! empty($_POST['pg_xml'])) {
            $data            = json_decode(json_encode((array)simplexml_load_string($_POST['pg_xml'])), true);
            $data['pg_type'] = 'add';
        }

        foreach ($data as $k => $v) {
            if (is_array($v) && empty($v)) {
                unset($data[$k]);
        }
        }

        $this->callbackRequest = new CallbackRequest($data);
        $this->callbackRequest->setMerchant($this->merchantId);
        $this->callbackRequest->setSecret($this->secret);

        return $this;
    }

    public function responseCallback(): string
    {
        return 'ok';
    }

    public function callbackAfterSetOperation(PaymentSystemTransaction $previousModel)
    {
    }

    public function hasMultiPayments(): bool
    {
        return true;
    }

    public function reverseCardBinding(PaymentSystemTransaction $model)
    {
        // TODO: Implement reverseCardBinding() method.
    }

    public function orderInfo(string $orderId)
    {
        $params           = [
            'pg_merchant_id' => $this->merchantId,
            'pg_payment_id'  => $orderId,
            'pg_salt'        => $this->generateRandomString(),
        ];
        $params['pg_sig'] = $this->signature($params, 'get_status2.php');

        $response = $this->transaction('/get_status2.php', $params);

        return $response->getResponse();
    }
}
