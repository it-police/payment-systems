<?php

namespace ITPolice\PaymentSystems\Systems\TarlanPayments;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use ITPolice\PaymentSystems\Interfaces\ServiceInterface;
use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Models\PaymentSystemTransaction;
use ITPolice\PaymentSystems\Traits\ServiceTrait;
use Illuminate\Http\Request;

class Service implements ServiceInterface
{
    use ServiceTrait;

    protected static $systemCode = 'tarlan';
    public static $refIdPrefix = 'ref-';

    /**
     * Метод привязки карты
     * @return Response
     * @throws \Exception
     */
    public function cardBinding()
    {
        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона');
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email');
        }

        $params = [
            'merchant_id' => $this->merchantId,
            'user_id'     => $this->userId,
            'request_url' => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl,
            'secret_key'  => $this->signature($this->merchantId . $this->userId),
        ];

        return $this->transaction('/api/invoice/card-linking', $params);
    }

    /**
     * Регистрация заказа
     * @param $data
     * @return Response
     * @throws \Exception
     */
    protected function register($data)
    {
        $data['currency'] = 643;
        $data['sector'] = $this->merchantId;
        $data['signature'] = $this->signature($data['amount'], $data['currency']);
        return $this->transaction('/webapi/Register', $data);
    }

    /**
     * Генерация цифровой подписи
     * @param mixed ...$args
     * @return string
     */
    protected function signature(...$args)
    {
        $signature = implode($args) . $this->secret;
        return bcrypt($signature, ['salt' => 10]);
        //return crypt($signature, 10);
    }

    /**
     * @param $method
     * @param $params
     * @return Response
     * @throws \Exception
     */
    protected function transaction($method, $params, $isPost = true)
    {
        $baseUrl = $this->getBaseUrl();

        $params['is_test'] = (bool)env('PAY_SYSTEM_TEST_MODE');
        $postParams = json_encode($params, JSON_UNESCAPED_UNICODE);

        $startTime = microtime(true);

        $url = $baseUrl . $method;

        $ch = curl_init();
        if ($isPost) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            $url .= '?' . http_build_query($params);
        }

        curl_setopt_array($ch, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 310,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSLVERSION     => 6,
            CURLOPT_HTTPHEADER     => array(
                "Content-Type: application/json",
                "Accept: application/json",
            ),
        ));
        $res = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $responseObj = json_decode($res);

        $requestExecutionTime = microtime(true) - $startTime;

        Log::debug('tarlan response ' . $method, [
            'params'   => $params,
            'httpCode' => $httpCode,
            'response' => (array)$responseObj,
            'execTime' => $requestExecutionTime
        ]);

        return new Response($responseObj);
    }

    /**
     * Получение базового URL
     * @return string
     */
    protected function getBaseUrl()
    {
        if (env('PAY_SYSTEM_TEST_MODE')) {
            $baseUrl = 'https://api.tarlanpayments.kz';
        } else {
            $baseUrl = 'https://api.tarlanpayments.kz';
        }

        return $baseUrl;
    }

    /**
     * Выдача средств
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function payoutByToken($orderId, $amount, $cardToken, $description = 'Выдача')
    {
//        TODO
    }

    /**
     * Оплата
     * @param $orderId
     * @param float $amount
     * @param string $description
     * @param float $fee
     * @return Response
     * @throws \Exception
     */
    public function pay($orderId, $amount, $description = '', $fee = 0)
    {
        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $amount = $amount + $fee;

        $params = [
            'reference_id' => $orderId,
            'request_url'  => $this->returnUrl ? $this->returnUrl : $this->defaultReturnUrl,
            'back_url'     => $this->callbackUrl,
            'description'  => $description,
            'amount'       => $amount,
            'merchant_id'  => $this->merchantId,
            'user_id'      => $this->userId,
            'user_email'   => $email,
            'secret_key'   => $this->signature($orderId)
        ];
        return $this->transaction('/invoice/create', $params);
    }

    /**
     * todo
     * Повторное списание средств на основе ID обычной оплаты
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return \ITPolice\PaymentSystems\Systems\MandarinBank\Response
     * @throws \Exception
     */
    public function recurring($orderId, $amount, $cardToken, $description = '')
    {
        // todo
    }

    /**
     * Безакцептное списание средств по токену
     * @param $orderId
     * @param $amount
     * @param $cardToken
     * @param string $description
     * @return Response
     * @throws \Exception
     */
    public function recurringByToken($orderId, $amount, $cardToken, $description = 'Регулярный платеж')
    {
        $phone = $this->preparePhone($this->phone);
        if (!$phone) {
            throw new \Exception('Не указан номер телефона', 1000);
        }

        $email = (string)$this->email;
        if (!$email) {
            $email = $this->defaultEmail;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Не корректный email', 1000);
        }

        $referenceId = self::$refIdPrefix . $orderId;
        $params = [
            'reference_id' => $referenceId,
            'back_url'     => $this->callbackUrl,
            'description'  => $description,
            'amount'       => $amount,
            'merchant_id'  => (int) $this->merchantId,
            'user_id'      => $this->userId,
            'card_id'      => $cardToken,
            'user_email'   => $email,
            'user_phone'   => $phone,
            'secret_key'   => $this->signature($referenceId)
        ];

        $response = $this->transaction('/api/invoice/api-recurrent', $params);
        $response->setId($referenceId);

        return $response;
    }

    public function getCardsPayIn(): array
    {
        $params = [
            'merchant_id' => (int) $this->merchantId,
            'user_id'     => $this->userId,
            'secret_key'  => $this->signature($this->merchantId . $this->userId)
        ];
        $response = $this->transaction('/api/cards/payin', $params, false);
        return $this->getCardModelsFromResponse($response);
    }

    public function getCardsPayOut(): array
    {
        $params = [
            'merchant_id' => $this->merchantId,
            'user_id'     => $this->userId,
            'secret_key'  => $this->signature($this->merchantId . $this->userId)
        ];
        $response = $this->transaction('/api/cards/payout', $params, false);
        return $this->getCardModelsFromResponse($response);
    }

    /**
     * @param $response
     * @return BankCard[]
     */
    protected function getCardModelsFromResponse($response)
    {
        $cards = [];
        foreach ((array)@$response->getResponse()->data as $cardData) {
            $bankCard = new BankCard();
            $mask = preg_replace("/[^\d]+/", '', $cardData->masked_pan);
            $bankCard->token = $cardData->id;
            $bankCard->bin = mb_substr($mask, 0, 6);
            $bankCard->pan = mb_substr($mask, -4);

            $cards[] = $bankCard;
        }

        return $cards;
    }

    public function setCallbackRequest(Request $request, $POST = null, $GET = null)
    {
        $data = json_decode($request->getContent(), true);
        foreach ($data as $k => $v) {
            if (is_array($v) && empty($v)) unset($data[$k]);
        }
        $this->callbackRequest = new CallbackRequest($data);
        $this->callbackRequest->setMerchant($this->merchantId);
        $this->callbackRequest->setSecret($this->secret);
        return $this;
    }

    public function responseCallback(): string
    {
        return 'ok';
    }

    public function callbackAfterSetOperation(PaymentSystemTransaction $previousModel)
    {

    }

    public function hasMultiPayments(): bool
    {
        return true;
    }

    public function reverseCardBinding(PaymentSystemTransaction $model)
    {
        // TODO: Implement reverseCardBinding() method.
    }

    public function orderInfo(string $orderId)
    {
        // TODO: Implement orderInfo() method.
    }
}
