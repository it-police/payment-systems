<?php

namespace ITPolice\PaymentSystems\Systems\TarlanPayments;


use ITPolice\PaymentSystems\Interfaces\ResponseInterface;
use ITPolice\PaymentSystems\Traits\ResponseTrait;

class Response implements ResponseInterface
{
    use ResponseTrait;

    public function parse($response)
    {
        $this->id = @$response->data->transaction_id;
        $this->link = @$response->data->redirect_url;
        $this->success = (bool) @$response->success;
        $this->errorMessage = (string) @$response->message;
    }
}
