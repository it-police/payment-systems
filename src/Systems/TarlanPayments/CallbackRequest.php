<?php

namespace ITPolice\PaymentSystems\Systems\TarlanPayments;

use ITPolice\PaymentSystems\Models\BankCard;
use ITPolice\PaymentSystems\Interfaces\CallbackRequestInterface;
use ITPolice\PaymentSystems\Traits\CallbackRequestTrait;

class CallbackRequest implements CallbackRequestInterface
{
    use CallbackRequestTrait;

    /**
     * Метод проверки подписи
     * @param array $request
     * @return bool
     */
    public function checkSign()
    {
        $request = $this->request;
        $sign = $request['secret_key'];
        $signature = $request['reference_id'].$this->secret;
        $verify = password_verify($signature, $sign);
        return password_verify($signature, $sign);
    }

    public function transactionId()
    {
        $transactionId = @$this->request['transaction_id'];
        $refId = @$this->request['reference_id'];
        if($refId && mb_strpos($refId, Service::$refIdPrefix) === 0) {
            $transactionId = $refId;
        }
        return $transactionId;
    }

    public function action()
    {
        $object_type = @$this->request['type'];
        if ($object_type == 0 || $object_type == 2) {
            return self::ACTION_PAY;
        } elseif ($object_type == 1 || $object_type == 3) {
            return self::ACTION_PAYOUT;
        } elseif ($object_type == 6) {
            return self::ACTION_CARD_BINDING;
        }
        return null;
    }

    public function errorMessage(): string
    {
        return (string) (@$this->request['description'] ?? @$this->request['message']);
    }

    public function amount(): float
    {
        return (float)@$this->request['amount'] / 100;
    }

    public function bankCard(): BankCard
    {
        // todo parse card
        $bankCard = new BankCard();

        $token = $this->request['token'];
        $cardBin = mb_substr($this->request['pan'], 0, 6);
        $cardPan = mb_substr($this->request['pan'], -4);
        list($cardMonth, $cardYear) = explode('/', $this->request['expdate']);
        $cardMonth = str_pad($cardMonth, 2, '0', STR_PAD_LEFT);
        $cardYear = (int)$cardYear;
        $cardHolder = $this->request['name'];

        $bankCard->token = $token;
        $bankCard->bin = $cardBin;
        $bankCard->pan = $cardPan;
        $bankCard->month = $cardMonth;
        $bankCard->year = $cardYear;
        $bankCard->holder = $cardHolder;

        return $bankCard;
    }

    public function isStopError(): bool
    {
        switch ((int)$this->errorCode()) {
            case 2:
            case 3:
                return true;
        }
        return false;
    }

    public function status()
    {
        $status = @$this->request['status'];
        switch ($status) {
            case 1:
                return self::STATUS_SUCCESS;
            case 4:
            case 5:
            case 6:
            //case 'TIMEOUT':
                return self::STATUS_ERROR;
            default:
                return $status;
        }
    }

    public function errorCode()
    {
        return @$this->request['reason_code'];
    }

    public function isFailedError(): bool
    {
        // todo
        return false;
    }
}
